'use strict'

angular.module 'cryptoinvestApp'
.controller 'AdminNavCtrl', ($scope, $location, Auth, $http) ->
  #alert $location.path()

  $scope.isSubPage = (route) ->
    $location.path() == '/admin' + route
