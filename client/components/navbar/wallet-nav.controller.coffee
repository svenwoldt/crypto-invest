'use strict'

angular.module 'cryptoinvestApp'
.controller 'walletNavCtrl', ($scope, $location, Auth, $http, $stateParams) ->
  #alert $location.path()
  console.log 'params: ', $stateParams

  $scope.isSubPage = (route) ->
    $location.path() == '/wallet/' + $stateParams.walletId + route
