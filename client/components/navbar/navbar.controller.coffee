'use strict'

angular.module 'cryptoinvestApp'
.controller 'NavbarCtrl', ($scope, $location, Auth) ->
  $scope.menu = [{
    title: 'BTC'
    link: '/costaverage'
  },{
    title: 'Portfolio'
    link: '/portfolio'
  },{
    title: 'Watchlist'
    link: '/watchlist'
  },{
    title: 'Wallet'
    link: '/wallet'
  }

  ]
  $scope.isCollapsed = true
  $scope.isLoggedIn = Auth.isLoggedIn
  $scope.isAdmin = Auth.isAdmin
  $scope.getCurrentUser = Auth.getCurrentUser

  $scope.logout = ->
    Auth.logout()
    $location.path '/login'

  $scope.isActive = (route) ->
    route is $location.path()
