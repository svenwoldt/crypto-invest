'use strict'

angular.module 'cryptoinvestApp'
.controller 'AdminMessagesCtrl', ['$scope', '$rootScope', '$http', 'Auth', 'User', 'socket', ($scope, $rootScope, $http, Auth, User, socket) ->
  $scope.maxPaginationSize = 5
  $scope.currentPage = 1
  $scope.numPerPage = 10
  watcher = null

  init = ->
    if watcher # unregister the watcher.
      watcher()
    $http.get '/api/contacts'
    .success (messages) ->
      $scope.messages = messages
      $scope.totalItems = messages.length
      console.log 'messages.length ', messages.length
      $scope.$watch "currentPage + numPerPage", ->
        begin = (($scope.currentPage - 1) * $scope.numPerPage)
        end = begin + $scope.numPerPage
        console.log 'begin ', begin
        console.log 'end ', end
        $scope.pagedMessages = $scope.messages.slice(begin, end)
        $scope.numberOfPages = Math.ceil($scope.totalItems / $scope.numPerPage)
        console.log 'numberOfPages ', $scope.numberOfPages
      socket.syncUpdates 'message', $scope.messages
  init()

  $scope.deleteMessage = (message) ->
#    console.log 'deleting ' , message
    $http.delete '/api/contacts/' + message._id
    _.remove $scope.messages, message

#  $scope.searchResults = []
#  $scope.results = []
#  $scope.searchQuery = ""

#  $scope.clearSearchResults = ->
#    $scope.searchResults = []

#  $scope.searchCmd = ->
#    if $scope.searchTxt.length < 3
#      $scope.clearSearchResults()
#      return
#    searchEnc = encodeURIComponent($scope.searchTxt)
#    $http.get('/api/users/search/' + searchEnc).success (searchResults) ->
#      console.log(searchResults)
#      $scope.searchResults = searchResults
#      socket.syncUpdates 'searchResult', $scope.searchResults


  $scope.$on '$destroy', ->
    socket.unsyncUpdates 'message'
#    socket.unsyncUpdates 'searchResult'

]
