'use strict'

angular.module 'cryptoinvestApp'
.controller 'AdminUsersCtrl', ['$scope', '$http', 'Auth', 'User', 'socket', ($scope, $http, Auth, User, socket) ->
  $scope.maxPaginationSize = 5
  $scope.currentPage = 1
  $scope.numPerPage = 10
  watcher = null

  init = ->
    if watcher # unregister the watcher.
      watcher()
    $http.get '/api/users'
    .success (users) ->
      $scope.users = users
      $scope.totalItems = users.length
      console.log 'users.length ', users.length
      watcher = $scope.$watch "currentPage + numPerPage", ->
        begin = (($scope.currentPage - 1) * $scope.numPerPage)
        end = begin + $scope.numPerPage
        console.log 'begin ', begin
        console.log 'end ', end

        $scope.pagedUsers = $scope.users.slice(begin, end)
        $scope.numberOfPages = Math.ceil($scope.totalItems / $scope.numPerPage)
        console.log 'numberOfPages ', $scope.numberOfPages
        socket.syncUpdates 'user', $scope.users

  init()


  $scope.deleteUser = (user) ->
#    console.log 'deleting ' , user
    User.remove id: user._id
    _.remove $scope.users, user

#  $scope.searchResults = []
#  $scope.results = []
#  $scope.searchQuery = ""
#
#  $scope.clearSearchResults = ->
#    $scope.searchResults = []
#
#  $scope.searchCmd = ->
#    if $scope.searchTxt.length < 3
#      $scope.clearSearchResults()
#      return
#    searchEnc = encodeURIComponent($scope.searchTxt)
#    $http.get('/api/users/search/' + searchEnc).success (searchResults) ->
#      console.log(searchResults)
#      $scope.searchResults = searchResults
#      socket.syncUpdates 'searchResult', $scope.searchResults


  $scope.$on '$destroy', ->
    socket.unsyncUpdates 'user'
#    socket.unsyncUpdates 'searchResult'

]
