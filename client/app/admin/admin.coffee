'use strict'

angular.module 'cryptoinvestApp'
.config ($stateProvider) ->
  $stateProvider
  .state 'admin',
    url: '/admin'
    templateUrl: 'app/admin/pages/users.html'
    controller: 'AdminUsersCtrl'
  .state 'messages',
    url: '/admin/messages'
    templateUrl: 'app/admin/pages/messages.html'
    controller: 'AdminMessagesCtrl'
