'use strict'

angular.module 'cryptoinvestApp'
.controller 'WatchlistCtrl', ($scope, $http, socket, Auth, $modal) ->
  $scope.changebase = ->
#    console.log 'got called'

  $scope.status =
    isopen: false

  user = Auth.getCurrentUser()
#  console.log 'user: ', user
#  console.log 'got called', user._id

  $scope.toggleDropdown = ($event) ->
    $event.preventDefault()
    $event.stopPropagation()
    $scope.status.isopen = !$scope.status.isopen

  $scope.newSymbol = []
  $scope.searchResults = []
  $scope.results = []
  $scope.searchQuery = ""
  $scope.maxPaginationSize = 5
  $scope.currentPage = 1
  $scope.numPerPage = 20
  watcher = null

  init = ->
    if watcher # unregister the watcher.
      watcher()
    $http.get('/api/things/btc').success (btc) ->
      $scope.btc = btc
#      console.log 'btc: ', btc
      socket.syncUpdates 'btc', $scope.btc
    $http.get('/api/watches').success (watchList) ->
#      console.log watchList
      $scope.totalItems = watchList.length - 1
      $scope.watchList = watchList
      watcher = $scope.$watch "currentPage + numPerPage", ->
        begin = (($scope.currentPage - 1) * $scope.numPerPage)
        end = begin + $scope.numPerPage
        $scope.pagedwatchList = $scope.watchList.slice(begin, end)
        $scope.numPages = Math.ceil $scope.totalItems / $scope.numPerPage
        socket.syncUpdates 'watch', $scope.watchList

  init()


  $scope.selectCurrency = (currencySymbol) ->
    $scope.newSymbol.symbol = currencySymbol
#    console.log 'currency symbol:', $scope.newSymbol.symbol
    $scope.clearSearchResults()
    $scope.searchTxt = currencySymbol

  $scope.clearSearchResults = ->
    $scope.searchResults = []

  $scope.searchCmd = ->
    if $scope.searchTxt.length < 1
      $scope.clearSearchResults()
      return
    searchEnc = encodeURIComponent($scope.searchTxt)
    $http.get('/api/things/search/' + searchEnc).success (searchResults) ->
#      console.log(searchResults)
      $scope.searchResults = _.values(searchResults)
      socket.syncUpdates 'searchResult', $scope.searchResults

  $scope.trackThing = ->
    return if $scope.newSymbol.symbol is ''
    $http.post '/api/watches',
      symbol: $scope.newSymbol.symbol
      userId: user._id
    $scope.newSymbol.symbol = ''
    $scope.searchTxt = ''
    init()

  $scope.deleteFromWatchList = (thing) ->
    $http.delete '/api/watches/' + thing._id
    init()

  $scope.$on '$destroy', ->
    socket.unsyncUpdates 'watch'
    socket.unsyncUpdates 'btc'

  $scope.openWatchlistModal = (item) ->
    modalInstance = $modal.open(
      templateUrl: "modalContent.html"
      controller: "ModalInstanceCtrl"
      windowClass: "animated fadeIn"
      size: "lg"
      resolve: item: ->
        item
    )

.controller "ModalInstanceCtrl", ($scope, $modalInstance, item, $http) ->
  $scope.item = item
  #  console.log item


  $scope.ok = (amount)->
#    console.log 'amount: ', amount
#    console.log 'untouched: ', $scope.item.amountUntouched
    if amount is undefined
      $modalInstance.close()
    else
      soldAmount = $scope.item.amountUntouched - amount
      #      console.log soldAmount
      if soldAmount < 0
        return $scope.error = "You can not sell more than you have..."
      else
        $scope.item.amount = soldAmount
        $http.put '/api/things/' + $scope.item._id, $scope.item

      $modalInstance.close()
