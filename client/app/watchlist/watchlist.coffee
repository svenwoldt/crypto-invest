'use strict'

angular.module 'cryptoinvestApp'
.config ($stateProvider) ->
  $stateProvider.state 'watchlist',
    url: '/watchlist'
    templateUrl: 'app/watchlist/watchlist.html'
    controller: 'WatchlistCtrl'
