'use strict'

angular.module 'cryptoinvestApp'
.controller 'WalletCtrl', ($scope, $http, $modal, $timeout, Graph, socket, toastr) ->
  $http.get '/api/wallets/user'
  .success (wallets) ->
    $scope.wallets = wallets
    console.log 'wallets on scope: ', $scope.wallets[$scope.wallets.length - 1].cumulativeBalanceUsd
    $scope.cumulativeBalance = $scope.wallets[$scope.wallets.length - 1]
    $scope.wallets.pop()
    console.log '$scope.wallets: ', $scope.wallets
    console.log '$scope.cumulativeBalance: ', $scope.cumulativeBalance
    socket.syncUpdates 'wallet', $scope.wallets, (event, wallet, wallets) ->
      console.log 'socket on wallet: ', wallet
      toastr.success('You just created '+ wallet.label , 'Success!')
      Graph.get (graphData) ->
        $scope.graphData = graphData

  $scope.$on '$destroy', ->
    socket.unsyncUpdates 'wallet'

  $http.get('/api/things/btc')
  .success (btcMarketData) ->
    $scope.btc = btcMarketData
    $scope.btc.price.usd = Math.round($scope.btc.price.usd * 100) / 100

  Graph.get (graphData) ->
    $scope.graphData = graphData
    console.log "graphdata: ", graphData


  $scope.createWallet = ->
    modalInstance = $modal.open(
      templateUrl: "modalWalletCreate.html"
      controller: "ModalWalletCreateCtrl"
      windowClass: "animated fadeIn"
      size: "lg"
    )
  $scope.cumulativeBuy = ->
    modalInstance = $modal.open(
      templateUrl: "modalCumulativeBuy.html"
      controller: "ModalCumulativeBuyCtrl"
      windowClass: "animated fadeIn"
      size: "lg"
      resolve:
        wallets: ->
          $scope.wallets
        cumulativeBalance: ->
          $scope.cumulativeBalance
        btcPrice: ->
          $scope.btc
    )
  $scope.cumulativeSell = ->
    modalInstance = $modal.open(
      templateUrl: "modalCumulativeSell.html"
      controller: "ModalCumulativeSellCtrl"
      windowClass: "animated fadeIn"
      size: "lg"
      resolve:
        wallets: ->
          $scope.wallets
        cumulativeBalance: ->
          $scope.cumulativeBalance
        btcPrice: ->
          $scope.btc
    )

.controller "ModalWalletCreateCtrl", ($scope, $modalInstance, $http, $ocLazyLoad, $location) ->
  $scope.wallet = {}
  $scope.wallet.color = '#1ab394'

  $scope.createWallet = (wallet) ->
    console.log wallet
    $http.post 'api/wallets', wallet
    $modalInstance.close()

  $scope.cancel = () ->
    $modalInstance.close()

.controller "ModalCumulativeBuyCtrl", ($scope, $modalInstance, wallets, cumulativeBalance, btcPrice, $http, $ocLazyLoad, $location) ->
  console.log cumulativeBalance, btcPrice
  $scope.wallets = wallets
  $scope.cumulativeBalance = cumulativeBalance
  $scope.btcPrice = btcPrice
  $scope.totalAmountUsd = 0
  $scope.totalAmountBtc = 0

  $scope.getAdjustedBalanceBtc = (wallet) ->
    if !wallet.adjustedAccountBalanceBtc
      return wallet.balanceBtc
    else
      return wallet.adjustedAccountBalanceBtc

  $scope.getAdjustedBalanceUsd = (wallet) ->
    if !wallet.adjustedAccountBalanceUsd
      return wallet.balanceUsd
    else
      return wallet.adjustedAccountBalanceUsd

  updateToTal = ->
    $scope.totalAmountUsd = 0
    $scope.totalAmountBtc = 0
    angular.forEach(wallets, (wallet) ->
      if !wallet.buyAmountUsd
        wallet.buyAmountUsd = 0
      $scope.totalAmountUsd += Number(wallet.buyAmountUsd)
      if !wallet.buyAmountBtc
        wallet.buyAmountBtc = 0
      $scope.totalAmountBtc += Number(wallet.buyAmountBtc)
    )

    ###
      Number(((Number(wallet.balanceBtc) + wallet.buyAmountBtc) * $scope.btcPrice.price.usd).toFixed(2))
      toFixed returns Type string, therefore it has to be converted back to a Number
    ###
  $scope.updateAmountBtc = (wallet) ->
    console.log wallet.buyAmountBtc
    console.log $scope.btcPrice.price.usd
    wallet.buyAmountBtc = Number((Number(wallet.buyAmountUsd) / Number($scope.btcPrice.price.usd)).toFixed(8))
    wallet.adjustedAccountBalanceUsd = Number(((Number(wallet.balanceBtc) + wallet.buyAmountBtc) * $scope.btcPrice.price.usd).toFixed(2))
    wallet.adjustedAccountBalanceBtc = Number(wallet.adjustedAccountBalanceUsd / $scope.btcPrice.price.usd).toFixed(8)
    updateToTal()

  $scope.updateAmountUsd = (wallet) ->
    console.log wallet.buyAmountBtc
    console.log $scope.btcPrice.price.usd
    wallet.buyAmountUsd = Number((Number(wallet.buyAmountBtc) * Number($scope.btcPrice.price.usd)).toFixed(2))
    wallet.adjustedAccountBalanceBtc = Number(((Number(wallet.balanceUsd) + wallet.buyAmountUsd) / $scope.btcPrice.price.usd).toFixed(8))
    wallet.adjustedAccountBalanceUsd = Number(wallet.adjustedAccountBalanceBtc * $scope.btcPrice.price.usd).toFixed(2)
    console.log wallet.adjustedAccountBalanceUsd, Number(wallet.balanceUsd), $scope.btcPrice.price.usd
    console.log wallet.adjustedAccountBalanceBtc, wallet.buyAmountUsd
    updateToTal()


  $scope.buy = () ->
    $modalInstance.close()

  $scope.cancel = () ->
    $modalInstance.close()

.controller "ModalCumulativeSellCtrl", ($scope, $modalInstance, wallets, cumulativeBalance, btcPrice, $http, $ocLazyLoad, $location) ->
  console.log cumulativeBalance, btcPrice
  $scope.wallets = wallets
  $scope.cumulativeBalance = cumulativeBalance
  $scope.btcPrice = btcPrice
  $scope.totalAmountUsd = 0
  $scope.totalAmountBtc = 0

  updateToTal = ->
    $scope.totalAmountUsd = 0
    $scope.totalAmountBtc = 0
    angular.forEach(wallets, (wallet) ->
      if !wallet.sellAmountUsd
        wallet.sellAmountUsd = 0
      $scope.totalAmountUsd += Number(wallet.sellAmountUsd)
      if !wallet.sellAmountBtc
        wallet.sellAmountBtc = 0
      $scope.totalAmountBtc += Number(wallet.sellAmountBtc)
    )

  $scope.getAdjustedBalanceBtc = (wallet) ->
    if !wallet.adjustedAccountBalanceBtc
      return wallet.balanceBtc
    else
      return wallet.adjustedAccountBalanceBtc

  $scope.getAdjustedBalanceUsd = (wallet) ->
    if !wallet.adjustedAccountBalanceUsd
      return wallet.balanceUsd
    else
      return wallet.adjustedAccountBalanceUsd

  $scope.updateAmountBtc = (wallet) ->
    console.log wallet.sellAmountBtc
    console.log $scope.btcPrice.price.usd
    wallet.sellAmountBtc = Number((Number(wallet.sellAmountUsd) / Number($scope.btcPrice.price.usd)).toFixed(8))
    wallet.adjustedAccountBalanceUsd = Number(((Number(wallet.balanceBtc) - wallet.sellAmountBtc) * $scope.btcPrice.price.usd).toFixed(2))
    wallet.adjustedAccountBalanceBtc = Number(wallet.adjustedAccountBalanceUsd / $scope.btcPrice.price.usd).toFixed(8)
    updateToTal()

  $scope.updateAmountUsd = (wallet) ->
    console.log wallet.sellAmountBtc
    console.log $scope.btcPrice.price.usd
    wallet.sellAmountUsd = Number((Number(wallet.sellAmountBtc) * Number($scope.btcPrice.price.usd)).toFixed(2))
    wallet.adjustedAccountBalanceBtc = Number(((Number(wallet.balanceUsd) - wallet.sellAmountUsd) / $scope.btcPrice.price.usd).toFixed(8))
    wallet.adjustedAccountBalanceUsd = Number(wallet.adjustedAccountBalanceBtc * $scope.btcPrice.price.usd).toFixed(2)
    console.log wallet.adjustedAccountBalanceUsd, Number(wallet.balanceUsd), $scope.btcPrice.price.usd
    console.log wallet.adjustedAccountBalanceBtc, wallet.sellAmountUsd
    updateToTal()


  $scope.sell = () ->
    $modalInstance.close()

  $scope.cancel = () ->
    $scope.wallets = []
    $scope.cumulativeBalance = {}
    $scope.btcPrice = {}
    $scope.totalAmountUsd = 0
    $scope.totalAmountBtc = 0
    $modalInstance.close()
