'use strict'

angular.module 'cryptoinvestApp'
.controller 'WalletSendCtrl', ($scope, $http, $modal, $timeout, $stateParams, $location) ->

  walletId = $stateParams.walletId
  $scope.walletId = $stateParams.walletId

  $scope.isSubPage = (route) ->
    $location.path() == '/wallet/' + walletId + route
  $scope.loaded = false
  $http.get '/api/wallets/' + walletId
  .success (wallet) ->
    $scope.wallet = wallet
    $scope.send = {}
    console.log 'wallet on scope: ', $scope.wallet
    $scope.send.btc = Number($scope.wallet.data.maxSpendable)
    if Number($scope.wallet.data.available_balance) == 0
      $scope.wallet.data.maxSpendable = 0
      $scope.wallet.data.maxSpendableUsd = 0
      $scope.wallet.data.amountPlusNetworkFee = 0
      $scope.wallet.data.estimatedNetworkFee = 0
      $scope.wallet.data.estimatedNetworkFeeUsd = 0
    else
      $scope.wallet.data.amountPlusNetworkFee = Number($scope.send.btc) + Number($scope.wallet.data.estimatedNetworkFee)
    $scope.send.address = $scope.wallet.data.address
    $scope.loaded = true

  $http.get '/api/wallets/' + walletId + '/sent'
  .success (sentTransactions) ->
    console.log 'sentTransactions: ', sentTransactions
    $scope.sentTransactions = sentTransactions.data.txs

  $http.get('/api/things/btc')
  .success (btcMarketData) ->
    $scope.btc = btcMarketData
    $scope.btc.price.usd = Math.round($scope.btc.price.usd * 100) / 100


  $scope.updateUsd = ->
    $scope.send.usd = Number($scope.send.btc) * Number($scope.btc.price.usd)
    $scope.send.usd = $scope.send.usd.toFixed(2)

  $scope.updateBtc = ->
    $scope.send.btc = Number($scope.send.usd) / Number($scope.btc.price.usd)

  $scope.amountPlusNetworkFee = ->
    $scope.wallet.data.amountPlusNetworkFee = $scope.send.btc + Number($scope.wallet.data.estimatedNetworkFee)
    $scope.wallet.data.amountPlusNetworkFee = Number($scope.wallet.data.amountPlusNetworkFee).toFixed(8)

  $scope.sendTransaction = ->
    obj =
      amount: $scope.send.btc,
      recipient: $scope.send.recipient
    $http.post('/api/wallets/' + walletId + '/send', obj).success (sendTransaction) ->
      console.log sendTransaction


