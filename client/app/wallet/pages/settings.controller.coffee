'use strict'

angular.module 'cryptoinvestApp'
.controller 'WalletSettingsCtrl', ($scope, $http, $stateParams, $location) ->
  walletId = $stateParams.walletId
  $scope.walletId = $stateParams.walletId
  $scope.isSubPage = (route) ->
    $location.path() == '/wallet/' + walletId + route

  $scope.loaded = false
  $http.get '/api/wallets/settings/' + walletId
  .success (wallet) ->
    $scope.wallet = wallet
    console.log wallet
    $scope.loaded = true

  $scope.updateWallet = (wallet) ->
    $http.put('/api/wallets/' + walletId, {label: wallet.newLabel, color: wallet.color})
    $location.path '/wallet'
