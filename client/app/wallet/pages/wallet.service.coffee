'use strict'

angular.module 'cryptoinvestApp'
.factory 'Graph', ($resource) ->
  $resource 'api/wallets/graph'
