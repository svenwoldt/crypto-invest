'use strict'

angular.module 'cryptoinvestApp'
.controller 'WalletReceiveCtrl', ($scope, $http, $modal, $timeout, $stateParams, $location) ->

  walletId = $stateParams.walletId
  $scope.walletId = $stateParams.walletId
  $scope.isSubPage = (route) ->
    $location.path() == '/wallet/' + walletId + route

  $scope.loaded = false
  $http.get '/api/wallets/' + walletId
  .success (wallet) ->
    $scope.wallet = wallet
    $scope.send = {}
    if Number($scope.wallet.data.available_balance) == 0
      $scope.wallet.data.maxSpendable = 0
      $scope.wallet.data.maxSpendableUsd = 0
      $scope.wallet.data.amountPlusNetworkFee = 0
      $scope.wallet.data.estimatedNetworkFee = 0
      $scope.wallet.data.estimatedNetworkFeeUsd = 0
    else
      $scope.wallet.data.amountPlusNetworkFee = Number($scope.send.btc) + Number($scope.wallet.data.estimatedNetworkFee)
      $scope.send.address = $scope.wallet.data.address
      $scope.loaded = true

      $scope.send.btc = Number($scope.wallet.data.maxSpendable)
      $scope.wallet.data.amountPlusNetworkFee = Number($scope.send.btc) + Number($scope.wallet.data.estimatedNetworkFee)
      $scope.send.address = $scope.wallet.data.balances[0].address
    console.log 'wallet on scope: ', $scope.wallet
    $scope.loaded = true

  $http.get '/api/wallets/' + walletId + '/received'
  .success (receivedTransactions) ->
    console.log 'receivedTransactions: ', receivedTransactions
    $scope.receivedTransactions = receivedTransactions.data.txs

  $http.get('/api/things/btc')
  .success (btcMarketData) ->
    $scope.btc = btcMarketData
    $scope.btc.price.usd = Math.round($scope.btc.price.usd * 100) / 100


  $scope.updateUsd = ->
    $scope.send.usd = (Number($scope.send.btc) * Number($scope.btc.price.usd)).toFixed(2)

  $scope.updateBtc = ->
    $scope.send.btc = Number($scope.send.usd) / Number($scope.btc.price.usd)


  $scope.createQr = (transaction) ->
    modalInstance = $modal.open(
      templateUrl: "modalContent.html"
      controller: "ModalQrCtrl"
      windowClass: "animated fadeIn"
      size: "lg"
      resolve:
        transaction: ->
          transaction
    )


.controller "ModalQrCtrl", ($scope, $modalInstance, $http, $ocLazyLoad, $location, transaction) ->

  $scope.transaction = transaction
  console.log transaction

  $scope.ok = () ->
    $modalInstance.close()



