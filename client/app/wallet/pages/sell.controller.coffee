'use strict'

angular.module 'cryptoinvestApp'
.controller 'WalletSellCtrl', ($scope, $http, $modal, $timeout, $stateParams, $location) ->
  walletId = $stateParams.walletId
  $scope.walletId = $stateParams.walletId
  $scope.isSubPage = (route) ->
    $location.path() == '/wallet/' + walletId + route
  $scope.updateUsd = ->
    $scope.send.usd = (Number($scope.send.btc) * Number($scope.btc.price.usd)).toFixed(2)

  $scope.updateBtc = ->
    $scope.send.btc = Number($scope.send.usd) / Number($scope.btc.price.usd)
