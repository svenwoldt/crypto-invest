'use strict'

angular.module 'cryptoinvestApp'
.controller 'WalletBuyCtrl', ($scope, $http, $modal, $location, $stateParams) ->
  walletId = $stateParams.walletId
  $scope.walletId = $stateParams.walletId
  $scope.isSubPage = (route) ->
    $location.path() == '/wallet/' + walletId + route

  $scope.buy = (receive) ->
    modalInstance = $modal.open(
      templateUrl: "modalContent.html"
      controller: "ModalBuyCtrl"
      windowClass: "animated fadeIn"
      size: "lg"
      resolve:
        receive: ->
          receive
    )
  $scope.updateUsd = ->
    $scope.send.usd = (Number($scope.send.btc) * Number($scope.btc.price.usd)).toFixed(2)

  $scope.updateBtc = ->
    $scope.send.btc = Number($scope.send.usd) / Number($scope.btc.price.usd)

  $scope.inputValue = 8

.controller "ModalBuyCtrl", ($scope, $modalInstance, $http, $ocLazyLoad, $location, receive) ->

  $scope.receive = receive
  console.log receive

  $scope.ok = () ->
    $modalInstance.close()
