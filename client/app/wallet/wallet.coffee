'use strict'

angular.module 'cryptoinvestApp'
.config ($stateProvider) ->
  $stateProvider.state 'wallet',
    url: '/wallet'
    templateUrl: 'app/wallet/pages/wallet.html'
    controller: 'WalletCtrl'


#    resolve:
#      ocLazyLoad: ($ocLazyLoad) ->
#        $ocLazyLoad.load [
#          "assets/js/arbor/jquery-ui-1.9.2.custom.min.js",
#          "assets/js/arbor/arbor.js",
#          "assets/js/arbor/arbor-tween.js",
#          "assets/js/arbor/graphics.js",
#          "assets/js/arbor/renderer.js"
#        ]
  .state 'buy',
    url: '/wallet/:walletId/buy'
    templateUrl: 'app/wallet/pages/buy.html'
    controller: 'WalletBuyCtrl'
  .state 'sell',
    url: '/wallet/:walletId/sell'
    templateUrl: 'app/wallet/pages/sell.html'
    controller: 'WalletSellCtrl'
  .state 'receive',
    url: '/wallet/:walletId/receive'
    templateUrl: 'app/wallet/pages/receive.html'
    controller: 'WalletReceiveCtrl'
  .state 'send',
    url: '/wallet/:walletId/send'
    templateUrl: 'app/wallet/pages/send.html'
    controller: 'WalletSendCtrl'
  .state 'walletSettings',
    url: '/wallet/:walletId/settings'
    templateUrl: 'app/wallet/pages/settings.html'
    controller: 'WalletSettingsCtrl'
