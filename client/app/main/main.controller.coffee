'use strict'

angular.module 'cryptoinvestApp'
.controller 'MainCtrl', ($scope, $http, socket, $modal) ->
  $scope.messageSent = false
  $scope.err = false

  $scope.setFalse = ->
    $scope.messageSent = false

  $scope.sendMessage = (message) ->
    $scope.err = false
    if $scope.user.name == '' or $scope.user.email == '' or $scope.user.message == ''
      $scope.error = 'Please provide all the fields.'
      $scope.err = true
    else
#      console.log message
      $scope.messageSent = true
      $http.post('/api/contacts', message)
      .success (resp) ->
        console.log resp
        $scope.user.name = ''
        $scope.user.email = ''
        $scope.user.message = ''
        $scope.form.name.$touched = false
        $scope.form.email.$touched = false
        $scope.form.message.$touched = false

