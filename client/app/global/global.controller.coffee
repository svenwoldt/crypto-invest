'use strict'

angular.module 'cryptoinvestApp'
.controller 'GlobalCtrl', ($scope, toastr, $http, socket, $log) ->
#  sits atop of index.html and is meant to control the whole app.
#  anything running in here runs everywhere on the site

  showToast = (notification) ->
    if Number(notification.amountReceived) > 0
      toastr.success('You received '+ notification.amountReceived + ' BTC', 'New Transaction!')
    else
      toastr.success('You send '+ notification.amountSent + ' BTC', 'New Transaction!')

  updateNotification = (notification) ->
    $http.put('api/notifications/' + notification._id, {seen: true}).success (updatedNotification) ->
      console.log 'update successful: ', updatedNotification

  $http.get('api/notifications').success (notifications) ->
    angular.forEach(notifications, (notification) ->
      console.log notification
      toastr.success('Jelle send you 3 BTC to Admins Expense Wallet', 'New Transaction received!')
      showToast(notification)
      updateNotification(notification)
    )
    $scope.notifications = notifications

    socket.syncUpdates 'notification', $scope.notifications, (event, notification, notifications) ->
      console.log notification
      if notification.seen == false
        showToast(notification)
        updateNotification(notification)



  $scope.$on '$destroy', ->
    socket.unsyncUpdates 'notification'


.config (toastrConfig) ->
  angular.extend toastrConfig,
    allowHtml: false
    autoDismiss: false
    closeButton: false
    closeHtml: "<button>&times;</button>"
    containerId: "toast-container"
    extendedTimeOut: 1000
    iconClasses:
      error: "toast-error"
      info: "toast-info"
      success: "toast-success"
      warning: "toast-warning"

    maxOpened: 0
    messageClass: "toast-message"
    newestOnTop: true
    onHidden: null
    onShown: null
    positionClass: "toast-top-right"
    preventDuplicates: false
    preventOpenDuplicates: false
    progressBar: false
    tapToDismiss: true
    target: "body"
    templates:
      toast: "directives/toast/toast.html"
      progressbar: "directives/progressbar/progressbar.html"

    timeOut: 5000
    titleClass: "toast-title"
    toastClass: "toast"

