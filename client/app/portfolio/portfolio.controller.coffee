'use strict'

angular.module 'cryptoinvestApp'
.controller 'PortfolioCtrl', ($scope, $http, socket, Auth, $modal, $log) ->
  $scope.changebase = ->
#    console.log 'got called'

  $scope.status =
    isopen: false

  user = Auth.getCurrentUser()
#  console.log 'user: ', user
#  console.log 'got called', user._id

  $scope.toggleDropdown = ($event) ->
    $event.preventDefault()
    $event.stopPropagation()
    $scope.status.isopen = !$scope.status.isopen

  init = ->
    $http.get('/api/things/btc').success (btc) ->
      btc.timestamp
      $scope.btc = btc
#      console.log 'btc: ', btc
#      socket.syncUpdates 'btc', $scope.btc
    $http.get('/api/things/portfolio').success (currencies) ->
      console.log 'currencies: ', currencies
      $scope.currencies = []
      $scope.marketData = []
      $scope.total = {}

      data = _.values(currencies)
      for c in data

        if c.symbol
          $scope.currencies.push c.currency
          $scope.marketData.push c.marketData

        else
          $scope.total = c
#          console.log $scope.currencies
#          console.log $scope.marketData
#          console.log $scope.total
#      socket.syncUpdates 'thing', $scope.currencies


  init()
  $scope.newSymbol = []
  $scope.searchResults = []
  $scope.results = []
  $scope.searchQuery = ""

  $scope.selectCurrency = (currencySymbol) ->
    $scope.newSymbol.symbol = currencySymbol
#    console.log 'currency symbol:', $scope.newSymbol.symbol
    $scope.clearSearchResults()
    $scope.searchTxt = currencySymbol

  $scope.clearSearchResults = ->
    $scope.searchResults = []

  $scope.searchCmd = ->
    if $scope.searchTxt.length < 1
      $scope.clearSearchResults()
      return
    searchEnc = encodeURIComponent($scope.searchTxt)
    $http.get('/api/things/search/' + searchEnc).success (searchResults) ->
#      console.log(searchResults)
      $scope.searchResults = _.values(searchResults)
      socket.syncUpdates 'searchResult', $scope.searchResults

  $scope.addThing = ->
    return if $scope.newSymbol.symbol is ''
    $http.post '/api/things',
      symbol: $scope.newSymbol.symbol
      amount: $scope.newSymbol.amount
      price: $scope.newSymbol.price
      userId: user._id

    $scope.newSymbol.symbol = ''
    $scope.newSymbol.amount = ''
    $scope.newSymbol.price = ''
    $scope.searchTxt = ''


  $scope.deleteThing = (thing) ->
    $http.delete '/api/things/' + thing._id

#  $scope.$watch 'currencies', (newValue, oldValue) ->
##    $log.info 'currencies has changed', newValue, oldValue
#    init()
#  , true


  $scope.$on '$destroy', ->
#    socket.unsyncUpdates 'thing'
#    socket.unsyncUpdates 'searchResult'
#    socket.unsyncUpdates 'btc'


  $scope.openSellModal = (item) ->
    modalInstance = $modal.open(
      templateUrl: "modalContent.html"
      controller: "ModalInstanceCtrl"
      windowClass: "animated fadeIn"
      size: "lg"
      resolve: item: ->
        item
    )

.controller "ModalInstanceCtrl", ($scope, $modalInstance, item, $http) ->
  $scope.item = item
#  console.log item


  $scope.ok = (amount)->
#    console.log 'amount: ', amount
#    console.log 'untouched: ', $scope.item.amountUntouched
    if amount is undefined
      $modalInstance.close()
    else
      soldAmount = $scope.item.amountUntouched - amount
#      console.log soldAmount
      if soldAmount < 0
        return $scope.error = "You can not sell more than you have..."
      else
        $scope.item.amount = soldAmount
        $http.put '/api/things/' + $scope.item._id, $scope.item

      $modalInstance.close()
