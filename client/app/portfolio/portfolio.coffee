'use strict'

angular.module 'cryptoinvestApp'
.config ($stateProvider) ->
  $stateProvider.state 'portfolio',
    url: '/portfolio'
    templateUrl: 'app/portfolio/portfolio.html'
    controller: 'PortfolioCtrl'
