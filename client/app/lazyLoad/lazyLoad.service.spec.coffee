'use strict'

describe 'Service: lazyLoad', ->

  # load the service's module
  beforeEach module 'cryptoinvestApp'

  # instantiate service
  lazyLoad = undefined
  beforeEach inject (_lazyLoad_) ->
    lazyLoad = _lazyLoad_

  it 'should do something', ->
    expect(!!lazyLoad).toBe true
