'use strict'

angular.module 'cryptoinvestApp'
.service 'lazyLoad', [
  '$q'
  '$timeout'
  ($q, $t) ->
    deferred = $q.defer()
    promise = deferred.promise

    @load = (files) ->
      angular.forEach files, (file) ->
        if file.indexOf('.js') > -1
          # script
          ((d, script) ->
            fDeferred = $q.defer()
            script = d.createElement('script')
            script.type = 'text/javascript'
            script.async = true

            script.onload = ->
              $t ->
                fDeferred.resolve()
                return
              return

            script.onerror = ->
              $t ->
                fDeferred.reject()
                return
              return

            promise = promise.then(->
              script.src = file
              d.getElementsByTagName('head')[0].appendChild script
              fDeferred.promise
            )
            return
          ) document
        return
      deferred.resolve()
      promise

    return
]
