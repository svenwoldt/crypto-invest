'use strict'

angular.module 'cryptoinvestApp'
.config ($stateProvider) ->
  $stateProvider.state 'btcdca',
    url: '/costaverage'
    templateUrl: 'app/btcdca/btcdca.html'
    controller: 'BtcdcaCtrl'


  $stateProvider.state 'archiveDetail',
    url: '/archive/:id'
    templateUrl: 'app/btcdca/btcdca.html'
    controller: 'BtcdcaCtrl'


#resolve: ocLazyLoad: ($ocLazyLoad) ->
#$ocLazyLoad.load [
#  {
#    serie: true
#    name: 'angular-flot'
#    files: [
#      'assets/js/flot/jquery.flot.js'
#      'assets/js/flot/jquery.flot.time.js'
#      'assets/js/flot/jquery.flot.tooltip.min.js'
#      'assets/js/flot/jquery.flot.spline.js'
#      'assets/js/flot/jquery.flot.resize.js'
#      'assets/js/flot/jquery.flot.pie.js'
#      'assets/js/flot/curvedLines.js'
#      'assets/js/flot/angular-flot.js'
#    ]
#  }
#]
