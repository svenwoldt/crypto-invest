'use strict'

angular.module 'cryptoinvestApp'
.controller 'BtcdcaCtrl', ($scope, $http, socket, Auth, $modal, $log, $stateParams) ->
  $scope.changebase = ->
#    console.log 'got called'
  $scope.currencyOptions = [{
      name: 'US-Dollar',
      code: 'usd'
    },{
      name: 'US-Dollar',
      code: 'usd'
    },{
      name: 'US-Dollar',
      code: 'usd'
    },{
      name: 'US-Dollar',
      code: 'usd'
    },{
      name: 'US-Dollar',
      code: 'usd'
    }]
  if $stateParams.id == undefined
    $scope.stateParam = true
  else
    $scope.stateParam = false

  $scope.status =
    isopen: false

  user = Auth.getCurrentUser()
  #  console.log 'user: ', user
  #  console.log 'got called', user._id

  $scope.toggleDropdown = ($event) ->
    $event.preventDefault()
    $event.stopPropagation()
    $scope.status.isopen = !$scope.status.isopen

  $scope.total = {}
  $scope.bitcoins = []
  $scope.maxPaginationSize = 5
  $scope.currentPage = 1
  $scope.numPerPage = 10
  watcher = null
  $scope.newBtc = {}

  $scope.today = ->
    $scope.newBtc.dt = new Date

  $scope.today()

  $scope.clear = ->
    $scope.newBtc.dt = null

  $scope.openDatepicker = ($event) ->
    $event.preventDefault()
    $event.stopPropagation()
    $scope.opened = true

  $scope.dateOptions =
    formatYear: 'yy'
    startingDay: 1

  $scope.formats = [
    'dd-MMMM-yyyy'
    'yyyy/MM/dd'
    'dd.MM.yyyy'
    'shortDate'
  ]
  $scope.format = $scope.formats[0]
  #get day of year
  whichDay = (dateString) ->
    daysOfWeek = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")
    daysOfWeek[new Date(dateString).getDay()]

#  Date.prototype.getWeek = ->
#    oneJan = new Date(@getFullYear(), 0, 1)
#    Math.ceil (((this - oneJan) / 86400000) + oneJan.getDay() + 1) / 7

  pushStartSnapshot = (today, startBtcSaldo, startFiatSaldo) ->
    console.log "pushing startsnapshot"
#    week = today.getWeek()
    $http.post '/api/snapshots',
      start: today
      userId: user._id
      backgroundColor: '#3498db'
      title: user.name + " Entries"
      startBtcSaldo: startBtcSaldo
      startFiatSaldo: startFiatSaldo
    .success (newSnapshot) ->
      console.log newSnapshot
      init()

    ###*
    # Flot chart data and options
    ###

  d1 = [
    [
      1262304000000
      6
    ]
    [
      1264982400000
      3057
    ]
    [
      1267401600000
      20434
    ]
    [
      1270080000000
      31982
    ]
    [
      1272672000000
      26602
    ]
    [
      1275350400000
      27826
    ]
    [
      1277942400000
      24302
    ]
    [
      1280620800000
      24237
    ]
    [
      1283299200000
      21004
    ]
    [
      1285891200000
      12144
    ]
    [
      1288569600000
      10577
    ]
    [
      1291161600000
      10295
    ]
  ]
  d2 = [
    [
      1262304000000
      5
    ]
    [
      1264982400000
      200
    ]
    [
      1267401600000
      1605
    ]
    [
      1270080000000
      6129
    ]
    [
      1272672000000
      11643
    ]
    [
      1275350400000
      19055
    ]
    [
      1277942400000
      30062
    ]
    [
      1280620800000
      39197
    ]
    [
      1283299200000
      37000
    ]
    [
      1285891200000
      27000
    ]
    [
      1288569600000
      21000
    ]
    [
      1291161600000
      17000
    ]
  ]
  $scope.flotChartData = [
    {
      label: 'Data 1'
      data: d1
      color: '#17a084'
    }
    {
      label: 'Data 2'
      data: d2
      color: '#127e68'
    }
  ]
  $scope.flotChartOptions =
    xaxis: tickDecimals: 0
    series:
      lines:
        show: true
        fill: true
        fillColor: colors: [
          { opacity: 1 }
          { opacity: 1 }
        ]
      points:
        width: 0.1
        show: false
    grid:
      show: false
      borderWidth: 0
    legend: show: false
  $scope.loaded = false

  init = ->
    if watcher # unregister the watcher.
      watcher()
    console.log '$stateParams.id ', $stateParams.id
    if $stateParams.id == undefined
      query = '/api/snapshots/last'
    else
      query = '/api/snapshots/' + $stateParams.id
    $http.get(query).success (snapshots) ->
      $scope.snapshots = snapshots
      console.log 'snapshots: ', snapshots
      if $stateParams.id == undefined and !snapshots[0]
        today = 1287316800000 #first btc transaction 17. Oktober 2009
        startBtcSaldo = 0
        startFiatSaldo = 0
        pushStartSnapshot(today, startBtcSaldo, startFiatSaldo)
      else if $stateParams.id == undefined and snapshots[0]
        console.log "current snapshot: ", snapshots[0]
        start = snapshots[0].start
        end = snapshots[0].end
        $scope.currentSnapshotId = snapshots[0]._id
        $scope.currentSnapshotTitle = snapshots[0].title

      else
        console.log "archive snapshot: ", snapshots.start
        start = snapshots.start
        end = snapshots.end
        $scope.currentSnapshotId = snapshots._id
        $scope.currentSnapshotTitle = snapshots.title
        # snapshot limit to last element in array
        # snap[0] provides start time for query limit on cashbook entries
        # else query param is null
        # snap[0].start provides limit to open cashbook on init
        # otherwise on close entry snap[0].end provides query limit
        console.log "is start"
      if $stateParams.id == undefined
        console.log "Param is start only "
        query = '/api/dcas/collection/' + $scope.currentSnapshotId
        $scope.minDate = start
        $scope.maxDate = $scope.newBtc.dt
      else
        console.log 'Param is start and end'
        query = '/api/dcas/collection/' + $scope.currentSnapshotId
        $scope.minDate = start
        $scope.maxDate = end
      $http.get(query).success (btc) ->
        console.log btc
        $scope.bitcoins = btc.bitcoins.reverse()
        console.log $scope.total

        $scope.total.btcIn = btc.totalBtcIn.toFixed(4)
        $scope.total.btcOut = btc.totalBtcOut.toFixed(4)
        $scope.total.fiatIn = btc.totalFiatIn
        $scope.total.fiatOut = btc.totalFiatOut
        $scope.total.purchaseDca = Math.round(btc.dca * 100) / 100
        $scope.totalItems = btc.bitcoins.length
        $scope.total.startBtcSaldo = btc.startBtcSaldo.toFixed(4)
        $scope.total.startFiatSaldo = btc.startFiatSaldo.toFixed(4)
        $scope.total.endBtcSaldo = btc.endBtcSaldo.toFixed(4)
        $scope.total.endFiatSaldo = btc.endFiatSaldo.toFixed(4)
        $scope.total.purchaseVwap = 210


        $scope.total.currentFiatValue = Math.round(btc.currentFiatValue * 100) / 100
#        if btc.profitability == null
#          $scope.total.profitability = 0
#          $scope.total.gain = 0
#        else
        $scope.total.profitability = btc.profitability
        $scope.total.gain = Math.round((btc.currentFiatValue - $scope.total.endFiatSaldo) * 100) / 100


        console.log '$scope.total ', $scope.total
        watcher = $scope.$watch "currentPage + numPerPage", ->
          begin = (($scope.currentPage - 1) * $scope.numPerPage)
          end = begin + $scope.numPerPage
          console.log 'begin ', begin
          console.log 'end ', end

          $scope.pagedBitcoins = $scope.bitcoins.slice(begin, end)
          $scope.numberOfPages = Math.ceil($scope.totalItems / $scope.numPerPage)
          console.log 'numberOfPages ', $scope.numberOfPages
          $http.get('/api/things/btc').success (btcMarketData) ->
            btcMarketData.timestamp
            $scope.btc = btcMarketData
            $scope.btc.price.usd = Math.round($scope.btc.price.usd * 100) / 100
            console.log 'btc: ', btcMarketData
            socket.syncUpdates 'bitcoins', $scope.bitcoins
            socket.syncUpdates 'total', $scope.total
            socket.syncUpdates 'btc', $scope.btc
            $scope.loaded = true


  $scope.endFiatSaldoMarker = ->
    if $scope.total.purchaseDca == 0
      return ''
    if $scope.btc.price.usd > $scope.total.purchaseDca
      return 'fa fa-plus'
    else
      return 'fa fa-minus'

  $scope.profitabilityMarker = ->
    if $scope.total.profitability == 0
      return ''
    if $scope.total.profitability > 0
      return 'fa fa-arrow-up'
    else
      return 'fa fa-arrow-down'

  $scope.endFiatSaldoMarkerColor = ->
    if $scope.btc.price.usd > $scope.total.purchaseDca
      return 'color: green'
    else
      return 'color: red'

  $scope.profitabilityMarkerColor = ->
    if $scope.total.profitability > 0
      return 'color: green'
    else
      return 'color: red'

  $scope.saveXEditEntry = (entry) ->
    entry.btcIn = if isNaN(entry.btcIn) then 0 else Number(entry.btcIn)
    entry.fiatOut = if isNaN(entry.fiatOut) then 0 else Number(entry.fiatOut)
    entry.btcOut = if isNaN(entry.btcOut) then 0 else Number(entry.btcOut)
    entry.fiatIn = if isNaN(entry.fiatIn) then 0 else Number(entry.fiatIn)
    $http.put '/api/dcas/' + entry._id, entry

  $scope.addBtc = ->
    if $scope.newBtc.btcIn is '' and $scope.newBtc.fiatOut is '' and $scope.newBtc.btcOut is '' and $scope.newBtc.fiatIn is ''
      return console.log 'nice try'
    $scope.newBtc.btcIn = if isNaN($scope.newBtc.btcIn) then 0 else Number($scope.newBtc.btcIn)
    $scope.newBtc.btcOut = if isNaN($scope.newBtc.btcOut) then 0 else Number($scope.newBtc.btcOut)
    if !$scope.newBtc.customId
      $scope.newBtc.customId = null
    if !$scope.newBtc.description
      $scope.newBtc.description = null
    $scope.newBtc.fiatOut = if isNaN($scope.newBtc.fiatOut) then 0 else Number($scope.newBtc.fiatOut)
    $scope.newBtc.fiatIn = if isNaN($scope.newBtc.fiatIn) then 0 else Number($scope.newBtc.fiatIn)
    $http.post '/api/dcas',
      description: $scope.newBtc.description
      customId: $scope.newBtc.customId
      btcIn: $scope.newBtc.btcIn
      fiatOut: $scope.newBtc.fiatOut
      btcOut: $scope.newBtc.btcOut
      fiatIn: $scope.newBtc.fiatIn
      time: $scope.newBtc.dt.getTime()
      userId: user._id
    $scope.newBtc.btcIn   = ''
    $scope.newBtc.fiatOut = ''
    $scope.newBtc.btcOut  = ''
    $scope.newBtc.fiatIn  = ''
    $scope.newBtc.description = ''
    $scope.newBtc.customId = ''
    init()

  $scope.closeEntry =  ->
    $http.put '/api/snapshots/' + $scope.currentSnapshotId,
      end: new Date().getTime()
      userId: user._id
    today = new Date().getTime()
    startBtcSaldo = $scope.total.endBtcSaldo
    startFiatSaldo = $scope.total.currentFiatValue
    pushStartSnapshot(today, startBtcSaldo, startFiatSaldo)

  $scope.deleteBtc = (bitcoin) ->
    $http.delete '/api/dcas/' + bitcoin._id
    init()

  $scope.$watch 'bitcoins', (newValue, oldValue) ->
#    $log.info 'dca has changed', newValue, oldValue
    init()
  , true


  $scope.$on '$destroy', ->
    socket.unsyncUpdates 'dca'
    socket.unsyncUpdates 'total'
    socket.unsyncUpdates 'bitcoins'
    socket.unsyncUpdates 'btc'


  $scope.openArchiveModal = (snapshots) ->
    modalInstance = $modal.open(
      templateUrl: "modalContent.html"
      controller: "ModalArchiveCtrl"
      windowClass: "animated fadeIn"
      size: "lg"
      resolve:
        lazyLoad: ($ocLazyLoad) ->
          $ocLazyLoad.load(["bower_components/fullcalendar/dist/fullcalendar.js"])
    )

.filter "reverse", ->
  (items) ->
    items.slice().reverse()

.filter "jsDate", ->
  (x) ->
    JSON.parse(x)

.filter "currency", ->
  (number, currencyCode) ->
    currency =
      USD: "$"
      BTC: "฿"
      GBP: "£"
      AUD: "$"
      EUR: "€"
      CAD: "$"
      MIXED: "~"

    thousand = undefined
    decimal = undefined
    format = undefined
    #changed if with else to be compatible with input box at the moment
    if $.inArray(currencyCode, [
      "USD"
      "AUD"
      "CAD"
      "MIXED"
    ]) >= 0
      thousand = "."
      decimal = ","
      format = "%s%v"
    else
      thousand = ","
      decimal = "."
      format = "%s%v"

    accounting.formatMoney number, currency[currencyCode], 2, thousand, decimal, format


.controller "ModalArchiveCtrl", ($scope, $modalInstance, $http, $ocLazyLoad, $location) ->
  $http.get('/api/snapshots').success (snapshots) ->
    $scope.snapshots = snapshots
    console.log snapshots

  $scope.redirect = (event) ->
    if isNaN(event.end)
      $location.path '/costaverage'
    else
      $location.path '/archive/' + event._id
    $modalInstance.close()

  $scope.ok = () ->
    $modalInstance.close()

.directive "cashbookFullCalendar", [ "$location", "$routeParams", ($location, $routeParams) ->
  restrict: "A"
  scope:
    options: "=fullCalendar"

  link: (scope, element, attr) ->
    calendar = undefined
    defaultOptions = undefined
    defaultOptions =
      header:
        left: "prev,next today"
        center: "title"
        right: ""

      selectable: true
      eventClick: (event, allDay, jsEvent, view) ->
        end = undefined
        start = undefined
        console.log "event :", event
#        if event.end isnt null
#          start = new Date(event.start).getTime()
#          end = new Date(event.end).getTime()
#          console.log "query between " + start + " and " + end
#          $location.path "/cbentry-archive/" + $routeParams.cashbookId + "/" + $routeParams.cashbookName + "/" + start + "/" + end
#        else
#          $location.path "/cbentry/" + $routeParams.cashbookId + "/" + $routeParams.cashbookName

      editable: true
      events: []
      buttonText:
        prev: "<i class=\"fa fa-angle-left\"></i>"
        next: "<i class=\"fa fa-angle-right\"></i>"
        prevYear: "<i class=\"fa fa-angle-double-left\"></i>"
        nextYear: "<i class=\"fa fa-angle-double-right\"></i>"
        today: "Today"
        month: "Month"
        week: "Week"
        day: "Day"

    $.extend true, defaultOptions, scope.options
    if defaultOptions.droppable is true
      defaultOptions.drop = (date, allDay) ->
        copiedEventObject = undefined
        originalEventObject = undefined
        originalEventObject = $(this).data("eventObject")
        copiedEventObject = $.extend({}, originalEventObject)
        copiedEventObject.start = date
        copiedEventObject.allDay = allDay
        calendar.fullCalendar "renderEvent", copiedEventObject, true
        $(this).remove()  if defaultOptions.removeDroppedEvent is true
    calendar = angular.element("calendar").cashbookFullCalendar(defaultOptions)
]
