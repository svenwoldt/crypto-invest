/**
 * Main application routes
 */

'use strict';

var errors = require('./components/errors');

module.exports = function(app) {

  // Insert routes below
  app.use('/api/emailAlerts', require('./api/emailAlert'));
  app.use('/api/exchanges', require('./api/exchange'));
  app.all('/api/*', function(req, res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
  });
  app.all('/auth/*', function(req,res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
  });
  app.use('/api/notifications', require('./api/notification'));
  app.use('/api/wallets', require('./api/wallet'));
  app.use('/api/historicalBtcs', require('./api/historicalBtc'));
  app.use('/api/snapshots', require('./api/snapshot'));
  app.use('/api/dcas', require('./api/dca'));
  app.use('/api/contacts', require('./api/contact'));
  app.use('/api/watches', require('./api/watch'));
  app.use('/api/things', require('./api/thing'));
  app.use('/api/users', require('./api/user'));

  app.use('/auth', require('./auth'));

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
   .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get(function(req, res) {
      res.sendfile(app.get('appPath') + '/index.html');
    });
};
