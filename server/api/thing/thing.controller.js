/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /things              ->  index
 * POST    /things              ->  create
 * GET     /things/:id          ->  show
 * PUT     /things/:id          ->  update
 * DELETE  /things/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Thing = require('./thing.model');
var Promise = require('promise');
var async = require('async');
var https = require('https');
var Promise = require('promise');
var lunr = require("lunr");
var marketIndex = {};
var marketMap = [];
var tempMarketMap = [];
var accounting = require('accounting');
var delayed = require('delayed');
var apiKeyBittrex = '1ec3ba6c4f9c4cb3bbdecfe34311b0bd';
var apiSecretBittrex = '6590a8a89964493d805ad968cc66181f';
var bittrex = require('node.bittrex.api');

bittrex.options({
  'apikey' : apiKeyBittrex,
  'apisecret' : apiSecretBittrex,
  'stream' : true,
  'verbose' : true,
  'cleartext' : false
});

//bittrex.getorderhistory({},function (response) {
//  //console.log('bittrex response: ', JSON.stringify(response));
//  response.result.forEach(function (order) {
//    console.log(JSON.stringify(order) + '\n')
//  });
//});
//bittrex.getbalances( function( data ) {
//  data.result.forEach(function (balance) {
//    console.log(JSON.stringify(balance) + '\n')
//  });
//});
function getMarketData(url) {
  return new Promise(function (fullfill, reject) {
    https.get(url, function (data) {
      var str = '';
      data.on('data', function (chunk) {
        //console.log('Response: ' + chunk);
        str += chunk;
      });

      // after appending all the data into one string we parse that into
      // JSON format and access the resourceId from https results[0]
      // finally we fullfill that initial promise
      data.on('end', function () {
        //console.log('on end: ', JSON.parse(str));
        fullfill(JSON.parse(str));

      }).on('error', function (e) {
        console.error(e);
        reject(e);
      });
    });
  });
}


// Initialize the search engine.
function createMarketIndex() {
  marketIndex = {};
  marketIndex = lunr(function () {
    this.field('symbol', {boost: 10});
    this.ref('ref');
  });
  var coinMcapUrl = 'https://coinmarketcap-nexuist.rhcloud.com/api/all';
  getMarketData(coinMcapUrl).then(function (marketData) {
    var arr = _.values(marketData);
    //console.log('array: ', arr.symbol);
    arr.forEach(function (entry) {
      //console.log("symbol: ", entry.timestamp);
      marketIndex.add({
        ref: entry.symbol,
        symbol: entry.symbol
      });
      // Map the original element by id.
      tempMarketMap[entry.symbol] = entry
    });
    marketMap = tempMarketMap;
  });
}

exports.getMarketMapSymbol = function(symbol) {
  return new Promise(function (fullfill, reject) {
    //console.log('marketMap[symbol]: ', marketMap[symbol]);
    if (!marketMap[symbol]) return reject('Symbol not in index');
    fullfill(marketMap[symbol]);
  })
};

function getMarketMapSymbol(symbol) {
  return new Promise(function (fullfill, reject) {
    //console.log('marketMap[symbol]: ', marketMap[symbol]);
    if (!marketMap[symbol]) return reject('Symbol not in index');
    fullfill(marketMap[symbol]);
  })
}

function initialize() {
  console.log('updating market data!!!');
  createMarketIndex();
  delayed.delay(initialize, 300000);
}

initialize();

// Get btc quotes
exports.btc = function(req, res) {
  var btc = marketMap['btc'];
  //console.log('btc: ', btc);
  console.log('btc: ', btc);
  return res.json(200, btc);
};

function calculateExchangeVwap(balance, adjusted) {
  return new Promise(function (fullfill, reject) {
    //console.log(balance.Currency);
    bittrex.getorderhistory({market: 'BTC-' + balance.Currency},function (response) {
      //console.log('bittrex response: ', JSON.stringify(response));
      adjusted.value_past = 0;
      response.result.forEach(function (order) {
        //console.log(JSON.stringify(order) + '\n');
        if (order.OrderType == 'LIMIT_BUY') {
          var value_past = order.Quantity * order.PricePerUnit;
          //console.log(value_past);

          adjusted.value_past += value_past;
        }
      });
      fullfill(adjusted);
    });
  });
}

function pushBittrexBalance(combined, total, balances, res) {
  console.log(balances);
  async.each(balances, function (balance, callback) {
    console.log(JSON.stringify(balance) + '\n');
    balance.symbol = balance.Currency.toLowerCase();
    if (balance.symbol == 'btc') return;
    var coinMcapUrl = 'https://coinmarketcap-nexuist.rhcloud.com/api/' + balance.symbol;
    getMarketData(coinMcapUrl).then(function (data) {
      var adjusted = {};
      adjusted.symbol = balance.symbol;
      adjusted.current_price = data.price.btc;
      adjusted.amount = balance.Available;
      adjusted.value_present = adjusted.amount * data.price.btc;
      adjusted.amountUntouched = balance.Available;
      calculateExchangeVwap(balance, adjusted).then(function (adjusted) {
        //console.log('test: ', adjusted);
        var object = {};
        object.symbol = balance.symbol;
        object.marketData = data;
        object.currency = adjusted;
        //console.log('data: ', data);
        if (!combined[balance.symbol]) {
          combined[balance.symbol] = object;
          //console.log(combined[balance.currency]);
        }
        else {
          //TODO merge the position if it already exists in the db
          console.log('entry already exists:');
        }
        console.log('next iteration: ', JSON.stringify(balance) + '\n');
        callback();
      });
    });
  }, function (err) {
    if (err) {

      // One of the iterations produced an error.
      // All processing will now stop.

      console.log('something went wrong with one of the answers');
      throw err;
    }
    console.log('finished:');
    total.value_change = total.value_present / total.value_past;
    total.value_percent_change = total.value_change * 100 - 100;
    combined['total'] = total;
    return res.json(200, combined);
  });
}


// Get list of users portfolio data
exports.index = function(req, res) {
  //console.log ('user: ', req.user);
  Thing.find({'userId': req.user._id}, function (err, currencies) {
    if (err) {
      return handleError(res, err);
    }
    if (!currencies) {
      return res.send(404, 'You do not have any valuta listed');
    }
    var data, total, object, combined, adjusted;
    total = {};
    combined = {};
    total.value_past = 0;
    total.value_present = 0;
    bittrex.getbalances(function (balances) {
      //console.log(total);
      balances.result.forEach(function (balance) {
        balance.symbol = balance.Currency.toLowerCase();
        if (balance.symbol == 'btc') return;
      });

    });
    async.each(currencies, function (currency, callback) {
      balances.result.forEach(function (balance) {
        balance.symbol = balance.Currency.toLowerCase();
        if (balance.symbol == 'btc') return;
        else if (balance.symbol == currency.symbol){
          merge(balance, currency)
        }
        {

        }
      });
      //console.log('this is the currency: ', currency);
      //console.log('this is the marketMap: ', marketMap[currency.symbol]);
      data = marketMap[currency.symbol];
      getMarketMapSymbol(currency.symbol).then(function (data){
        //console.log('btc price: ', data.price.btc);
        adjusted = {};
        //console.log(currency.amount);
        adjusted.value_past = currency.amount * Number(currency.price);
        //console.log(adjusted.value_past);
        adjusted.value_present = currency.amount * Number(data.price.btc);
        total.value_past += adjusted.value_past;
        total.value_present += adjusted.value_present;
        adjusted.value_change = adjusted.value_present / adjusted.value_past;
        adjusted.value_percent_change = adjusted.value_change * 100 - 100;
        adjusted.value_percent_change = Math.round(adjusted.value_percent_change * 100) / 100;
        data.change_on_investment = data.price.btc / currency.price;
        //console.log('change: ', data.change);
        data.percent_change = data.change_on_investment * 100 - 100;
        data.percent_change = Math.round(data.percent_change * 100) / 100;
        data.purchase_price = currency.price;
        adjusted._id = currency._id;
        adjusted.symbol = currency.symbol;
        adjusted.current_price = data.price.btc;
        adjusted.price = currency.price;
        adjusted.time = currency.time;
        adjusted.amount = currency.amount;
        adjusted.amountUntouched = currency.amount;
        object = {};
        object.symbol = currency.symbol;
        object.marketData = data;
        object.currency = adjusted;
        //console.log('data: ', data);
        combined[currency.symbol] = object;
        callback();
      });
    }, function (err) {
      if (err) {

        // One of the iterations produced an error.
        // All processing will now stop.

        console.log('something went wrong with one of the answers');
        throw err;
      }
      console.log('Well done :-)!');
      // All iterations of the async loop have finished
      bittrex.getbalances(function (balances) {
        //console.log(total);
        pushBittrexBalance(combined, total, balances.result, res)
      });
    });
  });
};


// Convert a search result element into an element to be presented to the client.
function createSearchResult(entry, res) {
  //console.log('element: ', entry);
  if (!entry.symbol){
    return res.json(404, 'Symbol could not be found')
  }
  else
    return {
      symbol: entry.symbol,
      position: entry.position,
      market_cap: entry.market_cap,
      price: entry.price,
      supply: entry.supply,
      volume: entry.volume,
      change: entry.change,
      timestamp: entry.timestamp
    };
}

exports.search = function(req, res) {
  var results;
  var filteredResults = [];
  console.log('searchTerm: ' + req.params.searchTerm);

  results = marketIndex.search(req.params.searchTerm);
  //console.log('results: ', results);

  results.forEach(function(entry) {
    //console.log('marketmap: ', marketMap[entry.ref]);
    if (!marketMap[entry.ref])
      return console.log('ref not in map');
    else
      filteredResults.push(createSearchResult(marketMap[entry.ref], res));
  });
  return res.json(filteredResults);
};

// Get a single thing
exports.show = function(req, res) {
  Thing.findById(req.params.id, function (err, thing) {
    if(err) { return handleError(res, err); }
    if(!thing) { return res.send(404); }
    return res.json(thing);
  });
};

// Creates a new portfolio entry in the DB.
exports.create = function(req, res) {
  req.body.time = new Date().getTime();
  console.log('req.user', req.user);
  Thing.find({userId: req.user._id}, function (err, things) {
    if (err) { return handleError(res, err); }
    if(!things[0]) {
      console.log('thing does not yet exist');
      Thing.create(req.body, function (err, newThing) {
        if (err) {
          return handleError(res, err);
        }
        return res.json(201, newThing);
      });
    }
    console.log('things: ', things.length);
    var i = 0;
    things.forEach(function (thing) {
      console.log('thing.symbol: ', thing.symbol);
      if(thing.symbol == req.body.symbol) {
        console.log('thing does exist applying vwap')
        //vwap formula
        var totalShares = Number(thing.amount) + Number(req.body.amount);
        var thingSum = thing.amount * thing.price;
        var reqSum = req.body.amount * req.body.price;
        var sum = thingSum + reqSum;
        var vwap = sum / totalShares;
        thing.amount = totalShares;
        thing.price = vwap;
        thing.time = req.body.time;
        thing.save(function (err) {
          if (err) { return handleError(res, err); }
          return res.json(200, thing);
        });
      }
      if(i == things.length - 1 && thing.symbol != req.body.symbol) {
        console.log('thing does not yet exist');
        Thing.create(req.body, function (err, newThing) {
          if (err) {
            return handleError(res, err);
          }
          return res.json(201, newThing);
        });
      }
      i++
    });
  });
}

// Updates an existing thing in the DB.
exports.update = function(req, res) {
  req.body.updateTime = new Date().getTime();
  if(req.body._id) { delete req.body._id; }
  Thing.findById(req.params.id, function (err, thing) {
    if (err) { return handleError(res, err); }
    if(!thing) { return res.send(404); }
    console.log('req.body.amount: ', req.body.amount);

    console.log('thing.amount: ', thing.amount);
    console.log('still some left? ', thing.amount - req.body.amount);
    if(req.body.amount == 0) {
      thing.remove(function(err) {
        if(err) { return handleError(res, err); }
        return res.send(204);
      });
    }
    else {
      var updated = _.merge(thing, req.body);
      updated.save(function (err) {
        if (err) { return handleError(res, err); }
        return res.json(200, thing);
      });
    }
  });
};

// Deletes a thing from the DB.
exports.destroy = function(req, res) {
  Thing.findById(req.params.id, function (err, thing) {
    if(err) { return handleError(res, err); }
    if(!thing) { return res.send(404); }
    thing.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
