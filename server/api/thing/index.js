'use strict';

var express = require('express');
var controller = require('./thing.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/btc', auth.isAuthenticated(), controller.btc);
router.get('/portfolio', auth.isAuthenticated(), controller.index);
//router.get('/:id', controller.show);
router.get('/search/:searchTerm', auth.isAuthenticated(), controller.search);
router.post('/', auth.isAuthenticated(), controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);


module.exports = router;
