'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ThingSchema = new Schema({
  symbol: String,
  amount: Number,
  price: Number,
  userId: String,
  time: Number,
  updateTime: Number
});

module.exports = mongoose.model('Thing', ThingSchema);
