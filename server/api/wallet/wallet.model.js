'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var WalletSchema = new Schema({
  label: String,
  userId: String,
  color: String,
  btcAddress: String
});

module.exports = mongoose.model('Wallet', WalletSchema);
