'use strict';

var express = require('express');
var controller = require('./wallet.controller');
var auth = require('../../auth/auth.service');
var router = express.Router();

router.get('/graph', auth.isAuthenticated(), controller.graphData);
router.get('/user', auth.isAuthenticated(), controller.showWallets);

router.get('/:walletId', auth.isAuthenticated(), controller.index);

router.get('/settings/:walletId', auth.isAuthenticated(), controller.showWallet);

router.get('/:walletId/received', auth.isAuthenticated(), controller.received);
router.get('/:walletId/sent', auth.isAuthenticated(), controller.sent);
router.post('/:walletId/send', auth.isAuthenticated(), controller.send);
//router.get('/:id', controller.show);
router.post('/', auth.isAuthenticated(), controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
