'use strict';

var _ = require('lodash');
var Wallet = require('./wallet.model');
var BlockIo = require('block_io');
var async = require('async');
var Promise = require('promise');
var version = 2; // API version
var Thing = require('../thing/thing.controller');
var btctestnetApiKey = '8526-fc12-00d4-0264';
var PIN = '10117359';
var block_io = new BlockIo(btctestnetApiKey, PIN, version);
var label;
//block_io.get_new_address({'label': label}, console.log);
//block_io.get_address_by_label({'address': '2N8Kxt1ZhwKWDkYN3CGuYCWrmi2o5yjQdAQ'}, console.log);
//block_io.get_network_fee_estimate({'amounts': '0.16', 'to_addresses': '2N8Kxt1ZhwKWDkYN3CGuYCWrmi2o5yjQdAQ'}, console.log);
//block_io.create_user({}, console.log);
// Get user wallet
exports.showWallet = function(req, res) {
  //console.log(req.user);
  Wallet.findById(req.params.walletId, function (err, wallet) {
    if (err) { return handleError(res, err); }
    if(!wallet) { return res.send(404); }
    return res.json(201, wallet);
  });
};

function getAddressBalance() {
  return new Promise(function (fullfill, reject) {
    console.log('marketMap[symbol]: ', marketMap[symbol]);
    if (!marketMap[symbol]) return reject('Symbol not in index');
    fullfill(marketMap[symbol]);
  });
}

// Get user wallets
exports.showWallets = function(req, res) {
  //console.log(req.user);
  Thing.getMarketMapSymbol('btc').then(function (btcData) {
    //console.log('btcData: ',btcData);
    Wallet.find({userId: req.user._id}, function (err, wallets) {
      if (err) {
        return handleError(res, err);
      }
      var combinedArray = [];
      var cumulativeBalanceUsd = 0, cumulativeBalanceBtc = 0;
      async.each(wallets, function (mongoWallet, callback) {
        block_io.get_address_balance({address: mongoWallet.btcAddress}, function (error, blockioWallet) {
          if (error) return console.log("Error occured:", error.message) && callback('Error occured fetching wallet.', error.message);
          console.log('balance data: ', blockioWallet.data);
          var combinedObj = {};
          combinedObj.balanceBtc = Number(blockioWallet.data.available_balance);
          combinedObj.balanceUsd = Math.round(btcData.price.usd * Number(blockioWallet.data.available_balance) * 100) / 100;
          combinedObj._id = mongoWallet._id;
          combinedObj.color = mongoWallet.color;
          combinedObj.btcAddress = mongoWallet.btcAddress;
          combinedObj.label = mongoWallet.label;
          combinedArray.push(combinedObj);
          cumulativeBalanceUsd = cumulativeBalanceUsd + combinedObj.balanceUsd;
          cumulativeBalanceBtc = cumulativeBalanceBtc + combinedObj.balanceBtc;
          callback();
        });
      }, function (err) {
        if (err) {

          // One of the iterations produced an error.
          // All processing will now stop.

          console.log('something went wrong with fetching the balance of one of the wallets');
          res.json(404);

        } else {
          combinedArray.push({
            cumulativeBalanceUsd: cumulativeBalanceUsd,
            cumulativeBalanceBtc: cumulativeBalanceBtc
          });
          //console.log('Well done :-)!', combinedArray);

          return res.json(201, combinedArray);
        }
      });
    });
  });
};
exports.getBalance = function(req, res) {
  //console.log(req.user);
  block_io.get_address_balance({address: wallet.btcAddress}, function (error, wallet) {
    if (error) return console.log("Error occured:", error.message);
  });
};


// Get blockIo included wallet
exports.index = function(req, res) {
  //console.log(req.user);
  Thing.getMarketMapSymbol('btc').then(function (btcData) {
    console.log('btcData: ',btcData);
  //console.log(btcData.price.usd);
    Wallet.findById(req.params.walletId, function (err, mongoWallet) {
      if (err) {
        return handleError(res, err);
      }
      if (!mongoWallet) {
        return res.send(404);
      }
      block_io.get_address_balance({address: mongoWallet.btcAddress}, function (error, wallet) {
        if (error) return console.log("Error occured:", error.message);
        block_io.get_network_fee_estimate({
          'amounts': String(wallet.data.available_balance),
          'to_addresses': '2N8Kxt1ZhwKWDkYN3CGuYCWrmi2o5yjQdAQ'
        }, function (error, feeEstimate) {
          //if (error) return console.log("Error occured:", error.message);
          //console.log('feeestimate: ', feeEstimate);
          var max_withdrawal_available = wallet.data.available_balance - feeEstimate.data.estimated_network_fee
          wallet.data.usd_balance = (btcData.price.usd * wallet.data.available_balance).toFixed(2);
          wallet.data.maxSpendable = max_withdrawal_available;
          wallet.data.maxSpendableUsd = (btcData.price.usd * max_withdrawal_available).toFixed(2);
          wallet.data.estimatedNetworkFee = feeEstimate.data.estimated_network_fee;
          wallet.data.estimatedNetworkFeeUsd = (btcData.price.usd * feeEstimate.data.estimated_network_fee).toFixed(2);
          //console.log(wallet);
          return res.json(201, wallet);
        });
      });
    });
  });

};

function getLabel(sender) {
  block_io.get_address_balance({'address': sender}, function (error, address) {
    if (error) return console.log("Error occured:", error.message);
    //console.log(address.data.balances[0].label);
    var senderDetail = {};
    senderDetail.address = sender;
    senderDetail.label = address.data.balances[0].label;
    return senderDetail
  });
}

// Get wallet transactions (received) for specific wallet
exports.received = function(req, res) {
  //console.log(req.user);
  Wallet.findById(req.params.walletId, function (err, wallet) {
    if (err) { return handleError(res, err); }
    if(!wallet) { return res.send(404); }
    block_io.get_transactions({type: 'received', addresses: String(wallet.btcAddress)}, function (error, transactions) {
      if (error) return console.log("Error occured:", error.message);
      console.log('received transactions: ', transactions);
      transactions.data.txs.forEach(function (transaction) {
        //console.log(transaction);
        transaction.sendersDetail = [];
        transaction.senders.forEach(function (sender) {
          //console.log(sender);
          transaction.sendersDetail.push(getLabel(sender));

        });
      });
      return res.json(201, transactions);
    });
  });
};

// Get wallet transactions (sent) for specific wallet
exports.sent = function(req, res) {
  console.log(req.user);
  Wallet.findById(req.params.walletId, function (err, wallet) {
    if (err) { return handleError(res, err); }
    if(!wallet) { return res.send(404); }
    block_io.get_transactions({type: 'sent', addresses: String(wallet.btcAddress)}, function (error, transactions) {
      if (error) return console.log("Error occured:", error.message);
      console.log('sent transactions: ', transactions);
      return res.json(201, transactions);
    });
  });
};

// Create a new transaction from specific users wallet
exports.send = function(req, res) {
  console.log(req.body);
  var btcAddressRecipient = req.body.recipient;
  var labelSender = req.user.email;
  Wallet.findById(req.params.walletId, function (err, wallet) {
    if (err) { return handleError(res, err); }
    if(!wallet) { return res.send(404); }
    block_io.withdraw_from_labels({amounts: String(req.body.amount), from_labels: String(labelSender), to_addresses: String(btcAddressRecipient)}, function (error, transaction) {
      if (error) return console.log("Error occured:", error.message);
      console.log('transactions successful: ', transaction);
      //transactions successful:  { status: 'success',
      //  data:
      //  { network: 'BTCTEST',
      //    txid: '1c749290bda81587e79804cf529b4d210f387bcaab468ee6ae0d77662ced395d',
      //    amount_withdrawn: '0.17470595',
      //    amount_sent: '0.17469595',
      //    network_fee: '0.00001000',
      //    blockio_fee: '0.00000000' } }

      return res.json(201, transaction);
    });
  });
};

// Creates a new wallet in the DB.
exports.create = function(req, res) {
  console.log(req.body);
  req.body.userId = req.user._id;
  block_io.get_new_address({}, function (error, block_io_wallet) {
    if (error) return console.log("Error occured:", error.message);
    console.log(block_io_wallet.data.address);
    req.body.btcAddress = block_io_wallet.data.address;
    Wallet.create(req.body, function (err, wallet) {
      if (err) {
        return handleError(res, err);
      }
      var callbackUrl = '145.100.198.131:9000/api/notifications/' + req.user._id;
      return res.json(201, wallet);
      //TODO winscho blocks other ports than 80 so callback url for notifications to grunt port 9000 not available
      //block_io.create_notification({'type': 'address', 'address': block_io_wallet.data.address, 'url': callbackUrl}, function (error, block_io_notification) {
      //  if (error) return console.log("Error creating a notifiaction occured:", error.message);
      //  console.log('notification blockIo: ', block_io_notification);
      //  return res.json(201, wallet);
      //});
    });
  });
};
// Export graph data of user wallets.
exports.graphData = function(req, res) {
  req.body.userId = req.user._id;
  Wallet.find({userId: req.user._id}, function (err, wallets) {
    if (err) {
      return handleError(res, err);
    }
    var graphData={
      "nodes":{
      },
      "edges":{
      }
    };
    var i = 1, j = 1;
    graphData.edges.main = {};
    wallets.forEach(function(wallet) {
      var nodeObj = {
        "color":wallet.color,
        "shape":"dot",
        "radius":30,
        "label":wallet.label,
        "alpha":1

      };
      var leafObj1, leafObj2, leafObj3, leafObj4, leafObj5;
      leafObj1 = {
        label: "Receice",
        color: "#1ab394",
        alpha: 0,
        link: 'wallet/' + wallet._id + '/receive'
      };
      leafObj2 = {
        label: "Send",
        color: "#1ab394",
        alpha: 0,
        link: 'wallet/' + wallet._id + '/send'
      };
      leafObj3 = {
        label: "Buy",
        color: "#1ab394",
        alpha: 0,
        link: 'wallet/' + wallet._id + '/buy'
      };
      leafObj4 = {
        label: "Sell",
        color: "#1ab394",
        alpha: 0,
        link: 'wallet/' + wallet._id + '/sell'
      };
      leafObj5 = {
        label: "Settings",
        color: "#1ab394",
        alpha: 0,
        link: 'wallet/' + wallet._id + '/settings'
      };
      var node = 'node' + i;
      graphData.nodes[node] = nodeObj;
      graphData.nodes['leaf' + j] = leafObj1;
      graphData.nodes['leaf' + (1+j)] = leafObj2;
      graphData.nodes['leaf' + (2+j)] = leafObj3;
      graphData.nodes['leaf' + (3+j)] = leafObj4;
      graphData.nodes['leaf' + (4+j)] = leafObj5;
      graphData.edges.main[node] = {};
      graphData.edges[node] = {};
      graphData.edges[node]['leaf' + j] = {};
      graphData.edges[node]['leaf' + (1+j)] = {};
      graphData.edges[node]['leaf' + (2+j)] = {};
      graphData.edges[node]['leaf' + (3+j)] = {};
      graphData.edges[node]['leaf' + (4+j)] = {};
      i++;
      j += 5;
    });

    graphData.nodes.main = {
      "color": "#1ab394",
      "shape": "dot",
      "radius": 39,
      "label": req.user.name,
      "alpha": 1
    };
    //console.log('graphData: ', graphData);
    return res.json(201, graphData);
  });
};
// Updates an existing wallet in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Wallet.findById(req.params.id, function (err, wallet) {
    if (err) { return handleError(res, err); }
    if(!wallet) { return res.send(404); }
    var updated = _.merge(wallet, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, wallet);
    });
  });
};

// Deletes a wallet from the DB.
exports.destroy = function(req, res) {
  Wallet.findById(req.params.id, function (err, wallet) {
    if(err) { return handleError(res, err); }
    if(!wallet) { return res.send(404); }
    wallet.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
