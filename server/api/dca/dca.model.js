'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var DcaSchema = new Schema({
  time: Number,
  description: String,
  customId: String,
  btcIn: Number,
  fiatOut: Number,
  btcOut: Number,
  fiatIn: Number,
  baseCurrency: String,
  userId: String,
  createdAt: Number
});

module.exports = mongoose.model('Dca', DcaSchema);
