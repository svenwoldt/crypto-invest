/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Dca = require('./dca.model');

exports.register = function(socket) {
  Dca.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Dca.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('dca:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('dca:remove', doc);
}