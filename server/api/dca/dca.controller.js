'use strict';

var _ = require('lodash');
var Dca = require('./dca.model');
var Thing = require('../thing/thing.controller');
var accounting = require('accounting');
var Snapshot = require('../snapshot/snapshot.model');

function calcSums(bitcoins, snapshot, res) {
  console.log('calculating sums!!!');
  var combined = {};
  Thing.getMarketMapSymbol('btc').then(function(btcData){
    combined.totalFiatIn = 0;
    combined.totalFiatOut = 0;
    combined.totalBtcIn = 0;
    combined.totalBtcOut = 0;

    bitcoins.forEach(function (bitcoin) {
      combined.totalFiatIn += bitcoin.fiatIn;
      combined.totalBtcOut += bitcoin.btcOut;
      combined.totalFiatOut += bitcoin.fiatOut;
      combined.totalBtcIn += bitcoin.btcIn;
    });
    // following if statements are for new creates snapshot.
    if(snapshot.startBtcSaldo == undefined){snapshot.startBtcSaldo = 0;}
    if(snapshot.startFiatSaldo == undefined){snapshot.startFiatSaldo = 0;}
    combined.bitcoins = bitcoins;
    combined.dca = combined.totalFiatOut / combined.totalBtcIn;
    combined.startBtcSaldo =  snapshot.startBtcSaldo;
    combined.startFiatSaldo = snapshot.startFiatSaldo;
    combined.endBtcSaldo = snapshot.startBtcSaldo + combined.totalBtcIn - combined.totalBtcOut;
    combined.endFiatSaldo = snapshot.startFiatSaldo + combined.fiatIn - combined.fiatOut;
    combined.currentFiatValue = combined.endBtcSaldo * btcData.price.usd;
    // TODO this cant be right?  the dca in a profitability measure?
    combined.changeOnInvestment = btcData.price.usd / combined.dca;
    combined.percentChange = combined.changeOnInvestment * 100 - 100;
    combined.profitability = Math.round(combined.percentChange * 100) / 100;

    return res.json(200, combined);

  });


}

exports.query = function(req, res){
  console.log("Getting all mutations for user.");
  console.log ('user: ', req.user);

  Dca.find({'userId': req.user._id}).exec(function (err, bitcoins) {
    if (err) {
      return handleError(res, err);
    }
    if (!bitcoins) {
      return res.send(404, 'You do not have any mutations yet');
    }
    calcSums(bitcoins, res);
  });
};

// Get list of currrent or archived dcas
exports.index = function(req, res) {
  Snapshot.findById(req.params.snapshotId, function (err, snapshot) {
    if(err) { return handleError(res, err); }
    if(!snapshot) { return res.send(404); }
    if (!snapshot.end) {
      // .where('time').gt(snapshot.start) OUT OF DCA QUERY on line below
      Dca.find({'userId': req.user._id}).exec(function (err, bitcoins) {
        if (err) {
          return handleError(res, err);
        }
        if (!bitcoins) {
          return res.send(404, 'You do not have any bitcoins yet');
        }
        calcSums(bitcoins, snapshot, res);
      });
    } else {
      Dca.find({'userId': req.user._id}).where('time').gt(snapshot.start).lt(snapshot.end).exec(function (err, bitcoins) {
        if (err) {
          return handleError(res, err);
        }
        if (!bitcoins) {
          return res.send(404, 'No bitcoins in the specified time interval');
        }
        calcSums(bitcoins, snapshot, res);
      });
    }
  });
};

// Get a single dca
exports.show = function(req, res) {
  Dca.findById(req.params.id, function (err, dca) {
    if(err) { return handleError(res, err); }
    if(!dca) { return res.send(404); }
    return res.json(dca);
  });
};

// Creates a new dca in the DB.
exports.create = function(req, res) {
  req.body.createdAt = new Date().getTime();
  console.log(req.body);
  Dca.create(req.body, function(err, dca) {
    if(err) { return handleError(res, err); }
    return res.json(201, dca);
  });
};

// Updates an existing dca in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Dca.findById(req.params.id, function (err, dca) {
    if (err) { return handleError(res, err); }
    if(!dca) { return res.send(404); }
    var updated = _.merge(dca, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, dca);
    });
  });
};

// Deletes a dca from the DB.
exports.destroy = function(req, res) {
  Dca.findById(req.params.id, function (err, dca) {
    if(err) { return handleError(res, err); }
    if(!dca) { return res.send(404); }
    dca.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
