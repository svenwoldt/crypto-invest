'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var EmailAlertSchema = new Schema({
  symbol: String,
  userId: String,
  active: Boolean,
  upperPrice: Number,
  lowerPrice: Number
});

module.exports = mongoose.model('EmailAlert', EmailAlertSchema);
