'use strict';

var _ = require('lodash');
var EmailAlert = require('./emailAlert.model');

// Get list of emailAlerts
exports.index = function(req, res) {
  EmailAlert.find(function (err, emailAlerts) {
    if(err) { return handleError(res, err); }
    return res.json(200, emailAlerts);
  });
};

// Get a single emailAlert
exports.show = function(req, res) {
  EmailAlert.findById(req.params.id, function (err, emailAlert) {
    if(err) { return handleError(res, err); }
    if(!emailAlert) { return res.send(404); }
    return res.json(emailAlert);
  });
};

// Creates a new emailAlert in the DB.
exports.create = function(req, res) {
  EmailAlert.create(req.body, function(err, emailAlert) {
    if(err) { return handleError(res, err); }
    return res.json(201, emailAlert);
  });
};

// Updates an existing emailAlert in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  EmailAlert.findById(req.params.id, function (err, emailAlert) {
    if (err) { return handleError(res, err); }
    if(!emailAlert) { return res.send(404); }
    var updated = _.merge(emailAlert, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, emailAlert);
    });
  });
};

// Deletes a emailAlert from the DB.
exports.destroy = function(req, res) {
  EmailAlert.findById(req.params.id, function (err, emailAlert) {
    if(err) { return handleError(res, err); }
    if(!emailAlert) { return res.send(404); }
    emailAlert.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}