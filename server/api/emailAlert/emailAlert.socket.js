/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var EmailAlert = require('./emailAlert.model');

exports.register = function(socket) {
  EmailAlert.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  EmailAlert.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('emailAlert:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('emailAlert:remove', doc);
}