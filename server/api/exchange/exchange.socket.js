/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Exchange = require('./exchange.model');

exports.register = function(socket) {
  Exchange.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Exchange.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('exchange:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('exchange:remove', doc);
}