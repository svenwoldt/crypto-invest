'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ExchangeSchema = new Schema({
  name: String,
  info: String,
  active: Boolean
});

module.exports = mongoose.model('Exchange', ExchangeSchema);