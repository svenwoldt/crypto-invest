'use strict';

var _ = require('lodash');

var Exchange = require('./exchange.model');

var Poloniex = require('poloniex.js');
var apiKeyPoloniex = 'QQ68AANR-QEIJD4BF-IE6JZW3H-Z0N543RK';
var apiSecretPoloniex = '1b4fe0a94ae65544d14b5ac95cab7f483ae7fc50278d8e2bde75960ae06148031a67087a5d0d73eed07b878c52aa8eadafce5ca7cd02fc314a77cf9ffd746126';
var poloniex = new Poloniex(apiKeyPoloniex, apiSecretPoloniex);

//var apiKeyBittrex = '1ec3ba6c4f9c4cb3bbdecfe34311b0bd';
//var apiSecretBittrex = '6590a8a89964493d805ad968cc66181f';
//var bittrex = require('node.bittrex.api');
//
//bittrex.options({
//  'apikey' : apiKeyBittrex,
//  'apisecret' : apiSecretBittrex,
//  'stream' : true,
//  'verbose' : true,
//  'cleartext' : false
//});
//
//bittrex.getorderhistory({},function (response) {
//  //console.log('bittrex response: ', JSON.stringify(response));
//  response.result.forEach(function (order) {
//    console.log(JSON.stringify(order) + '\n')
//  });
//});
//bittrex.getbalances( function( data ) {
//  console.log( data );
//});

// Get list of exchanges
exports.index = function(req, res) {
  Exchange.find(function (err, exchanges) {
    if(err) { return handleError(res, err); }
    return res.json(200, exchanges);
  });
};

// Get a single exchange
exports.show = function(req, res) {
  Exchange.findById(req.params.id, function (err, exchange) {
    if(err) { return handleError(res, err); }
    if(!exchange) { return res.send(404); }
    return res.json(exchange);
  });
};

// Creates a new exchange in the DB.
exports.create = function(req, res) {
  Exchange.create(req.body, function(err, exchange) {
    if(err) { return handleError(res, err); }
    return res.json(201, exchange);
  });
};

// Updates an existing exchange in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Exchange.findById(req.params.id, function (err, exchange) {
    if (err) { return handleError(res, err); }
    if(!exchange) { return res.send(404); }
    var updated = _.merge(exchange, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, exchange);
    });
  });
};

// Deletes a exchange from the DB.
exports.destroy = function(req, res) {
  Exchange.findById(req.params.id, function (err, exchange) {
    if(err) { return handleError(res, err); }
    if(!exchange) { return res.send(404); }
    exchange.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
