/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Watch = require('./watch.model');

exports.register = function(socket) {
  Watch.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Watch.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('watch:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('watch:remove', doc);
}