'use strict';

var _ = require('lodash');
var Watch = require('./watch.model');
var Thing = require('../thing/thing.controller');
var async = require('async');
var accounting = require('accounting');
var moment = require('moment');

// Get list of users portfolio data
exports.index = function(req, res) {
  console.log(req.user);
  Watch.find({'userId': req.user._id}, function (err, watchList) {
    if (err) {
      return handleError(res, err);
    }
    if (!watchList) {
      return res.send(404, 'You do not have any valuta in the watchList');
    }
    var data, combined;
    combined = {};
    async.each(watchList, function (item, callback) {
      console.log('this is the currency in the watchList: ', item);
      Thing.getMarketMapSymbol(item.symbol).then(function (data) {
        console.log('data: ', data);
        data.market_cap.usd = accounting.formatMoney(data.market_cap.usd);
        data.volume.usd = accounting.formatMoney(data.volume.usd);
        data.supply = accounting.formatMoney(data.supply, "", 0, ",");
        data._id = item._id;
        data.timeStamp = moment(data.timeStamp, "MM-DD-YYYY:hh:mm:ss");
        console.log('data: ', moment(data.timeStamp, "MM-DD-YYYY"));
        combined[item.symbol] = data;
        callback();
      });
    }, function (err) {
      if (err) {

        // One of the iterations produced an error.
        // All processing will now stop.

        console.log('something went wrong with one of the answers');
        throw err;
      }
      console.log('Well done :-)!');

      // All iterations of the async loop have finished
      //console.log(combined);
      return res.json(200, combined);
    });
  });
}


// Get a single watch
exports.show = function(req, res) {
  Watch.findById(req.params.id, function (err, watch) {
    if(err) { return handleError(res, err); }
    if(!watch) { return res.send(404); }
    return res.json(watch);
  });
};

// Creates a new track item in the DB.
exports.create = function(req, res) {
  req.body.time = new Date().getTime();
  Watch.create(req.body, function(err, thing) {
    if(err) { return handleError(res, err); }
    return res.json(201, thing);
  });
};

// Updates an existing watch in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Watch.findById(req.params.id, function (err, watch) {
    if (err) { return handleError(res, err); }
    if(!watch) { return res.send(404); }
    var updated = _.merge(watch, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, watch);
    });
  });
};

// Deletes a watch from the DB.
exports.destroy = function(req, res) {
  Watch.findById(req.params.id, function (err, watch) {
    if(err) { return handleError(res, err); }
    if(!watch) { return res.send(404); }
    watch.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
