'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var WatchSchema = new Schema({
  symbol: String,
  userId: String,
  time: Number
});

module.exports = mongoose.model('Watch', WatchSchema);
