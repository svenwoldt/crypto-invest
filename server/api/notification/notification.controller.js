'use strict';

var _ = require('lodash');
var Notification = require('./notification.model');

// Get list of notifications
exports.index = function(req, res) {
  Notification.find({userId: req.user._id, seen: {$ne: true}}, function (err, notifications) {
    if(err) { return handleError(res, err); }
    return res.json(200, notifications);
  });
};

// Get a single notification
exports.show = function(req, res) {
  Notification.findById(req.params.id, function (err, notification) {
    if(err) { return handleError(res, err); }
    if(!notification) { return res.send(404); }
    return res.json(notification);
  });
};

// Creates a new notification on blockIo for specific user about balance changes of his wallet address.
exports.createBlockIoNotification = function(req, res) {
  block_io.create_notification({'type': 'address', 'address': req.params.btcAddress, 'url': 'http://145.100.198.131/api/notifications/' + req.user._id}, function (error, block_io_notification) {
    if (error) return console.log("Error occured:", error.message);
    console.log('notification blockIo: ', block_io_notification);
  });
};

// Callback URL for blockIo
exports.createUserNotification = function(req, res) {
  console.log(req);
  req.body = {
    "notification_id": "5548827b507765fe0192e3b2",
    "delivery_attempt": 1,
    "created_at": 1426104819,
    "type": "address",
    "data": {
      "network": "BTC",
      "address": "2MvXiU1jNTw1JEehqe4vjc6gEC8AWNByj8e",
      "balance_change": "0.01000000", // net balance change, can be negative
      "amount_sent": "0.00000000",
      "amount_received": "0.01000000",
      "txid": "7af5cf9f2", // the transaction's identifier (hash)
      "confirmations": 1, // see below
      "is_green": false // was the transaction sent by a green address?
    }
  };
  var blockIoNotification = {};
  blockIoNotification.notificationId = req.body.notification_id;
  blockIoNotification.txId = req.body.data.txid;
  blockIoNotification.btcAddress = req.body.data.address;
  blockIoNotification.amountReceived = req.body.data.amount_received;
  blockIoNotification.amountSent = req.body.data.amount_sent;
  blockIoNotification.isGreen = req.body.data.is_green;
  blockIoNotification.userId = req.user._id;
  blockIoNotification.seen = false;
  Notification.create(blockIoNotification, function (err, notification) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(201, notification);
  });
};

// Updates an existing notification in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Notification.findById(req.params.id, function (err, notification) {
    if (err) { return handleError(res, err); }
    if(!notification) { return res.send(404); }
    var updated = _.merge(notification, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, notification);
    });
  });
};

// Deletes a notification from the DB.
exports.destroy = function(req, res) {
  Notification.findById(req.params.id, function (err, notification) {
    if(err) { return handleError(res, err); }
    if(!notification) { return res.send(404); }
    notification.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
