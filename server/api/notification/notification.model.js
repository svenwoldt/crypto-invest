'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var NotificationSchema = new Schema({
  userId: String,
  blockIoNotificationId: String,
  btcAddress: String,
  amountReceived: String,
  amountSent: String,
  isGreen: Boolean,
  txId: String,
  seen: Boolean
});

module.exports = mongoose.model('Notification', NotificationSchema);
