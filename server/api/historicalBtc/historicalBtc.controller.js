'use strict';

var _ = require('lodash');
var HistoricalBtc = require('./historicalBtc.model');
var csv = require('csv');
var Promise = require('promise');
var request = require('ajax-request');
var https   = require('https');
var fs      = require('fs');

var supportedCurrencies = [
  'USD',
  'EUR',
  'CNY',
  'CAD',
  'RUB'
];
var options = {
  hostname  : 'api.bitcoinaverage.com',
  path      : '/history/USD/per_day_all_time_history.csv',
  method    : 'GET'
};
function getHistoricalMarketData() {
  var file = fs.createWriteStream("BTC-USD.csv");
  var req = https.request(options, function(res) {
    console.log("statusCode: ", res.statusCode);
    console.log("headers: ", res.headers);
    res.on('data', function(d) {
      file.write(d);
    });
  });
  req.end();

  req.on('error', function(e) {
    console.error(e);
  });
}



function createHistoricalData() {
  console.log('supportedCurrencies: ', supportedCurrencies[0]);
  var url = 'https://api.bitcoinaverage.com/history/USD/per_day_all_time_history.csv';
  getHistoricalMarketData();
  //console.log('data: ', file);
  //csv.parse(file, function(err, parsedData){
  //  if(err) { return console.log('error: ', err); }
  //  console.log('parsedData: ', parsedData);
  //});

}

createHistoricalData();

// Get list of historicalBtcs
exports.index = function(req, res) {
  HistoricalBtc.find(function (err, historicalBtcs) {
    if(err) { return handleError(res, err); }
    return res.json(200, historicalBtcs);
  });
};

// Get a single historicalBtc
exports.show = function(req, res) {
  HistoricalBtc.findById(req.params.id, function (err, historicalBtc) {
    if(err) { return handleError(res, err); }
    if(!historicalBtc) { return res.send(404); }
    return res.json(historicalBtc);
  });
};

// Creates a new historicalBtc in the DB.
exports.create = function(req, res) {
  HistoricalBtc.create(req.body, function(err, historicalBtc) {
    if(err) { return handleError(res, err); }
    return res.json(201, historicalBtc);
  });
};

// Updates an existing historicalBtc in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  HistoricalBtc.findById(req.params.id, function (err, historicalBtc) {
    if (err) { return handleError(res, err); }
    if(!historicalBtc) { return res.send(404); }
    var updated = _.merge(historicalBtc, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, historicalBtc);
    });
  });
};

// Deletes a historicalBtc from the DB.
exports.destroy = function(req, res) {
  HistoricalBtc.findById(req.params.id, function (err, historicalBtc) {
    if(err) { return handleError(res, err); }
    if(!historicalBtc) { return res.send(404); }
    historicalBtc.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
