/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var HistoricalBtc = require('./historicalBtc.model');

exports.register = function(socket) {
  HistoricalBtc.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  HistoricalBtc.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('historicalBtc:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('historicalBtc:remove', doc);
}