'use strict';

var _ = require('lodash');
var Snapshot = require('./snapshot.model');

// Get list of snapshots
exports.index = function(req, res) {
  Snapshot.find({userId: req.user._id}).sort('-_id').exec(function (err, snapshots) {
    if(err) { return handleError(res, err); }
    if (snapshots[0] == undefined) {
      Snapshot.create(req.body, function(err, snapshot) {
        if(err) { return handleError(res, err); }
        snapshots[0] = snapshot;
        return res.json(201, snapshots);
      });
    } else {
      return res.json(200, snapshots);
    }
  });
};
// Get last element of snapshots
exports.indexLast = function(req, res) {
  Snapshot.find({userId: req.user._id}).sort('-_id').limit(2).exec(function (err, snapshots) {
    if(err) { return handleError(res, err); }
    return res.json(200, snapshots);
  });
};

// Get a single snapshot
exports.show = function(req, res) {
  Snapshot.findById(req.params.id, function (err, snapshot) {
    if(err) { return handleError(res, err); }
    if(!snapshot) { return res.send(404); }
    return res.json(snapshot);
  });
};

// Creates a new snapshot in the DB.
exports.create = function(req, res) {
  Snapshot.create(req.body, function(err, snapshot) {
    if(err) { return handleError(res, err); }
    return res.json(201, snapshot);
  });
};

// Updates an existing snapshot in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Snapshot.findById(req.params.id, function (err, snapshot) {
    if (err) { return handleError(res, err); }
    if(!snapshot) { return res.send(404); }
    var updated = _.merge(snapshot, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, snapshot);
    });
  });
};

// Deletes a snapshot from the DB.
exports.destroy = function(req, res) {
  Snapshot.findById(req.params.id, function (err, snapshot) {
    if(err) { return handleError(res, err); }
    if(!snapshot) { return res.send(404); }
    snapshot.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
