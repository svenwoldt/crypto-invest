'use strict';

var express = require('express');
var controller = require('./snapshot.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/last', auth.isAuthenticated(), controller.indexLast);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.get('/', auth.isAuthenticated(), controller.index);
router.post('/', auth.isAuthenticated(), controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
