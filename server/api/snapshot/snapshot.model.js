'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var SnapshotSchema = new Schema({
  title: String,
  start: Number,
  end: Number,
  userId: String,
  backgroundColor: String,
  startBtcSaldo: Number,
  startFiatSaldo: Number

});

module.exports = mongoose.model('Snapshot', SnapshotSchema);
