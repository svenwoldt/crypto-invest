// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in mutationServices.js
// 'starter.controllers' is found in mutationControllers.js
angular.module('cryptoinvest', ['ionic',
                                'ngCookies',
                                'ngResource',
                                'cryptoinvest.mutationControllers',
                                'cryptoinvest.mutationServices',
                                'cryptoinvest.priceService'
])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $httpProvider, $provide) {
  $ionicConfigProvider.navBar.alignTitle("center");

  $provide.factory("configParams", function(){
    return {
      host: 'https://cryptoinvest-10111001.rhcloud.com'
      //host: '/api'
      //host: 'http://192.168.1.114:9000'
    }
  });

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in mutationControllers.js
  $stateProvider
  .state('login', {
    url: '/login',
    templateUrl: 'app/login.html',
    controller: 'LoginCtrl'
  })
  .state('register', {
    url: '/register',
    templateUrl: 'app/register.html',
    controller: 'RegisterCtrl'
  })
  .state('onboarding', {
    url: '/onboarding',
    templateUrl: 'app/onboarding.html',
    controller: 'OnboardingCtrl'
  })
  // setup an abstract state for the tabs directive
  .state('tab', {
    url: "/tab",
    abstract: true,
    templateUrl: "app/tabs.html"
  })

  // Each tab has its own nav history stack:

  .state('tab.add-mutation', {
    url: '/add-mutation',
    views: {
      'tab-add-mutation': {
        templateUrl: 'mutation/tab-add-mutation.html',
        controller: 'AddMutationCtrl'
      }
    }
  })

  .state('tab.overview', {
      url: '/overview',
      views: {
        'tab-overview': {
          templateUrl: 'mutation/tab-overview.html',
          resolve: {
            Mutation: 'Mutation',
            Snapshot: 'Snapshot',
            snapshot: function(Snapshot){
              return Snapshot.get().$promise;
            },
            mutations: function(Mutation, snapshot){
              return Mutation.get({collection: 'collection', id: snapshot[0]._id}).$promise;
            }
          },
          controller: 'OverviewCtrl'
        }
      }
    })
    .state('tab.mutation-detail', {
      url: '/ovedcrview/mutation/:mutationId',
      views: {
        'tab-overview': {
          templateUrl: 'mutation/mutation-detail.html',
          controller: 'MutationDetailCtrl',
          params: ['mutationId'],
          resolve: {
            Mutation: 'Mutation',
            mutation: function(Mutation, $stateParams){
              return Mutation.get({id: $stateParams.mutationId}).$promise;
            }
          }
        }
      }
    })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'app/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

  $httpProvider.interceptors.push('authInterceptor');
})
.factory('authInterceptor', function($rootScope, $q, $window) {
  return {
    request: function(config) {
      config.headers = config.headers || {};
      if ($window.sessionStorage.token) {
        config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
      }
      return config;
    },
    responseError: function(response) {
      if (response.status === 401) {
        $window.sessionStorage.token = {};
      }
      return $q.reject(response);
    }
  };
})
.controller('LoginCtrl', function($scope, Auth, $state, $ionicHistory){
  $ionicHistory.nextViewOptions({
    disableBack: true
  });
  $scope.user = {};
  $scope.error = {};

  //   following method is for development and testing purposes
  $scope.setUC = function(email, password){
    $scope.user.email = email;
    $scope.user.password = password;
  };
  // ---------------------------

  $scope.login = function(form){
    $scope.submitted = true;
    $scope.errors = {};
    if (form.$valid){
      return Auth.login({
        email: $scope.user.email,
        password: $scope.user.password
      }).then(function(data) {
        console.log(data);
        Auth.getCurrentUser().$promise.then(function(user){
          console.log(user);
          if(user.onboarding){
            return $state.go('onboarding');
          }
          return $state.go('tab.overview');
        });
      })["catch"](function(err) {
        console.log(err);
        return $scope.errors.other = err.message;
      });
    }
  }
})
.controller('RegisterCtrl', function($scope, Auth, $state, $ionicHistory){
    $ionicHistory.nextViewOptions({
      disableBack: true
    });
    $scope.user = {};
    $scope.errors;
    $scope.register = function(form) {
      $scope.submitted = true;
      console.log(form.$valid);
      if (form.$valid) {
        return Auth.createUser({
          name: $scope.user.name,
          email: $scope.user.email,
          password: $scope.user.password
        }).then(function() {
          console.log($scope.user);
          Auth.login({email: $scope.user.email, password: $scope.user.password })
            .then(function(){
              $state.go('tab.overview');
              $scope.user = {};
            }
          );
        })["catch"](function(err) {
          console.log(err);
          return $scope.errors = err.statusText;
        });
      }
    };
})
.controller('AccountCtrl', function($scope, Auth, $state, $timeout) {
    $scope.$on("$ionicView.enter", function(scopes, states){
      if(states.stateName == "tab.account"){
        Auth.logout();
        $state.go('login');
      }
    });

})
.controller('OnboardingCtrl', function($scope, $state, Auth, User, $ionicSlideBoxDelegate){

  $scope.finishOnboarding = function(){
    var user = Auth.getCurrentUser();
    console.log(user);
    User.onboarded({id: user._id, controller: 'onboarding'}, function(res){console.log(res)});
    $state.go('tab.overview');
  };
  $scope.previous = function(){
    $ionicSlideBoxDelegate.previous();
  };
  $scope.next = function(){
    $ionicSlideBoxDelegate.next();
  };
    $scope.slideChanged = function(index){
      $scope.slideIndex = index;
    }

})
  // form watcher for validation on i.e. register form
.directive('formDrctv', function() {
  return function(scope, el, attrs) {
    if(attrs.name.$submitted){
      scope.$watch(attrs.name+'.$error', function(newVal, oldVal) {
      }, true);
    }
  }
});

