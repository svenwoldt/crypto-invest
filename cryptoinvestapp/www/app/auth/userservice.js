(function() {
  'use strict';

  angular.module('cryptoinvest').factory('User', function($resource, configParams) {
    return $resource(configParams.host + '/api/users/:id/:controller', {
      id: '@_id'
    }, {
      changePassword: {
        method: 'PUT',
        params: {
          controller: 'password'
        }
      },
      get: {
        method: 'GET',
        params: {
          id: 'me'
        }
      },
      onboarded: {
        method: 'PUT',
        params: {
          controller: 'onboarding'
        }

      }
    });
  });

}).call(this);
