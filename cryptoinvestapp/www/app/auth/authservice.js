(function() {
  'use strict';

  angular.module('cryptoinvest').factory('Auth', function($location, $rootScope, $http, User, $window, $q, configParams) {
    var currentUser;
    currentUser = $window.sessionStorage.token ? User.get() : {};
    return {
      /*
       Authenticate user and save token

       @param  {Object}   user     - login info
       @param  {Function} callback - optional
       @return {Promise}
       */

      login: function(user, callback) {
        var deferred,
          _this = this;
        deferred = $q.defer();
        $http.post(configParams.host + '/auth/local', {
          email: user.email,
          password: user.password
        }).success(function(data) {
          $window.sessionStorage.token = data.token;
          currentUser = User.get();
          deferred.resolve(data);
          return typeof callback === "function" ? callback() : void 0;
        }).error(function(err) {
          console.log(err.message);
          delete $window.sessionStorage.token;
          _this.logout();
          deferred.reject(err);
          return typeof callback === "function" ? callback(err.message) : void 0;
        });
        return deferred.promise;
      },
      /*
       Delete access token and user info

       @param  {Function}
       */

      logout: function() {
        delete $window.sessionStorage.token;
        currentUser = {};
      },
      /*
       Create a new user

       @param  {Object}   user     - user info
       @param  {Function} callback - optional
       @return {Promise}
       */

      createUser: function(user, callback) {
        var _this = this;
        return User.save(user, function(data) {
          return typeof callback === "function" ? callback("success") : void 0;
        }, function(err) {
          _this.logout();
          return typeof callback === "function" ? callback(err) : void 0;
        }).$promise;
      },
      /*
       Change password

       @param  {String}   oldPassword
       @param  {String}   newPassword
       @param  {Function} callback    - optional
       @return {Promise}
       */

      changePassword: function(oldPassword, newPassword, callback) {
        return User.changePassword({
          id: currentUser._id
        }, {
          oldPassword: oldPassword,
          newPassword: newPassword
        }, function(user) {
          return typeof callback === "function" ? callback(user) : void 0;
        }, function(err) {
          return typeof callback === "function" ? callback(err) : void 0;
        }).$promise;
      },
      /*
       Gets all available info on authenticated user

       @return {Object} user
       */

      getCurrentUser: function() {
        return currentUser;
      },
      /*
       Check if a user is logged in synchronously

       @return {Boolean}
       */

      isLoggedIn: function() {
        return currentUser.hasOwnProperty('role');
      },
      /*
       Waits for currentUser to resolve before checking if user is logged in
       */

      isLoggedInAsync: function(callback) {
        if (currentUser.hasOwnProperty('$promise')) {
          return currentUser.$promise.then(function() {
            if (typeof callback === "function") {
              callback(true);
            }
          })["catch"](function() {
            if (typeof callback === "function") {
              callback(false);
            }
          });
        } else {
          return typeof callback === "function" ? callback(currentUser.hasOwnProperty('role')) : void 0;
        }
      },
      /*
       Check if a user is an admin

       @return {Boolean}
       */

      isAdmin: function() {
        return currentUser.role === 'admin';
      },
      isUser: function() {
        return currentUser.role === 'user';
      },
      isApprover: function() {
        return currentUser.role === 'approver';
      },
      /*
       Get auth token
       */

      getToken: function() {
        return $window.sessionStorage.token;
      }
    };
  });

}).call(this);
