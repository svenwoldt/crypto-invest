angular.module('cryptoinvest.mutationControllers', [])

.controller('AddMutationCtrl', function($scope, Mutation, PriceService, Auth, $timeout, $state) {
  $scope.mutation = {};
  $scope.lastMutation = {};
  $scope.lastMutation.recent = false;
  $scope.init = PriceService.all().then(function(res){
    $scope.prices = res.data.price;
    $scope.exchangeRate = Number(res.data.price.usd);
    $scope.mutation.usd = Number(res.data.price.usd);
    $scope.mutation.btc = Number(res.data.price.btc);
  });
  $scope.updateUsd = function(){
    $scope.mutation.usd = $scope.mutation.btc * $scope.exchangeRate;
  };
  $scope.updateBtc = function(){
    $scope.mutation.btc = $scope.mutation.usd / $scope.exchangeRate;
  };

  $scope.sell = function(mutation){
    var mutationDto = {};
    mutationDto.btcOut = mutation.btc;
    mutationDto.fiatIn = mutation.usd;
    mutationDto.btcIn = 0;
    mutationDto.fiatOut = 0;
    mutationDto.baseCurrency = "usd";
    var user = Auth.getCurrentUser();
    mutationDto.userId = user._id;
    mutationDto.time = new Date().getTime();
    var mutation = new Mutation();
    mutation = mutationDto;
    Mutation.save(mutation, function(res){
      $scope.mutation.btc = Number($scope.prices.btc);
      $scope.mutation.usd = Number($scope.prices.usd);
      mutation.cardstyle = "sell";
      setLastMutation(mutation);
    })
  };

  $scope.buy = function(mutation){
    var mutationDto = {};
    mutationDto.btcIn = mutation.btc;
    mutationDto.fiatOut = mutation.usd;
    mutationDto.btcOut = 0;
    mutationDto.fiatIn = 0;
    mutationDto.baseCurrency = "usd";
    mutationDto.description = "Mutation registered through app.";
    var user = Auth.getCurrentUser();
    mutationDto.userId = user._id;
    mutationDto.time = new Date().getTime();
    var mutation = new Mutation();
    mutation = mutationDto;
    Mutation.save(mutation, function(res){
      $scope.mutation.btc = Number($scope.prices.btc);
      $scope.mutation.usd = Number($scope.prices.usd);
      mutation.cardstyle = "buy";
      setLastMutation(mutation);
    });
  };
  function setLastMutation(mutation){
    $scope.lastMutation = mutation;
    $scope.showLastMutation = true;
    $timeout(function(){$scope.showLastMutation = false;}, 3000);
  }

  $scope.goToOverview = function(){
    $state.go('tab.overview');
  }
})

.controller('OverviewCtrl', function($scope, Mutation, mutations, Snapshot, snapshot, PriceService, $state, $timeout) {
    $scope.$on("$ionicView.enter", function(scopes, states){
      if(states.fromCache && states.stateName == "tab.overview"){
        $scope.refresh();
      }
    });

    $scope.showPercentage  = true;
    $scope.changePortfolioProfitMetric = function(){
      if($scope.showPercentage){
        $scope.showPercentage = false;
      }
    };

    $scope.mutations = mutations;
    console.log(mutations);
    PriceService.all().then(function(res){
      // TODO PriceService seems slow
      $scope.exchangeRate = Number(res.data.price.usd);
    });

    $scope.refresh = function(){
      $scope.mutations = Mutation.get({collection: 'collection', id: snapshot[0]._id});
      $scope.mutations.$promise.then(function(data){
        $scope.mutations = data;
        // broadcast to ion-refresher to change state on ui
        $scope.$broadcast('scroll.refreshComplete');
      });
    };

    $scope.remove = function (mutation) {
      Mutation.delete({collection: mutation._id}).$promise.then(function(res){
        $timeout($scope.refresh(), 500);
      });
    };

    $scope.goToDetail = function(id){
      $state.go('tab.mutation-detail', {mutationId: id});
    };

    $scope.goToAddMutation = function(){
      $state.go('tab.add-mutation');
    };

    $scope.profitView = "percentage";
    $scope.changeProfitView = function(){
      if($scope.profitView === "percentage"){
        $scope.profitView = "absUsd";
      } else {
        $scope.profitView = "percentage";
      }
    };

})

.controller('MutationDetailCtrl', function($scope, $stateParams, Mutation, mutation, $ionicHistory) {
  $scope.mutation = mutation;
  $scope.delete = function(mutation){
    Mutation.delete({collection: mutation._id}).$promise.then(function(res){
      // resolve so that overview refreshes;
      $ionicHistory.goBack();
    });
  }
});

