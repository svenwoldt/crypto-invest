angular.module('cryptoinvest.priceService', [])

  .factory('PriceService', function(configParams, $http) {

    var prices = $http.get(configParams.host + "/api/things/btc").success(function(data){
      return data.price;
    }).error(function(err){
      console.log(err);
    });

    return {
      all: function() {
        return prices;
      },
      btc: function() {
        return prices.btc;
      },
      usd: function(){
        return prices.usd;
      }
    };
  });
