angular.module('cryptoinvest.mutationServices', [])

.factory('Mutation', [ 'configParams', '$resource', function(configParams, $resource) {
    return $resource(configParams.host + '/api/dcas/:collection/:id', {collection: '@collection', id: '@id'}, {
      query: {
        method: 'GET'
      }
    });
}])
.factory('Snapshot', function(configParams, $resource){
    return $resource(configParams.host + '/api/snapshots/:id', {id: '@id'},{
      get: {
        method: 'GET',
        isArray: true
      }
    });
  });

